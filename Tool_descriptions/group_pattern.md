### group_pattern : 


#### General description : 

Both coverage and specificity are calculated as percentages and computed with the genes count per gene pattern. 
Some patterns regroup only few genes and may bias the coverage and specificity distributions. 
Therefore, this module proposes to delete gene patterns having small counts and/or gather them 
with their opposed patterns, on the hypothesis that opposed patterns are regulated oppositely.

#### Arguments : 

| Arguments | Explanations | Examples |
| --- | --- | --- |
| Relations_table | Path to the relation (TF-gene) table, mus have following columns label ('Transcription_Factor', 'Transcription_Factor', 'Gene', 'Pattern_Gene', 'Pattern_TF', 'Reg'). |  work_directory/Relations_tables/my_Relations_table.csv |
| genes_per_gene_pattern_count_table | Path to the file containing the count of genes per gene's pattern, with columns labels 'Pattern_Gene', 'Nb_genes_per_gene_pattern' (.csv). | work_directory/genes_per_gene_pattern_count_tables/genes_per_gene_pattern_count_table.csv |
| follow_process_path | Path to file storing following process data (.md) | /home/user/workdirectory/Account.md |
| sep_table | String that separates columns in tables | '\t' |
| output_relations_group_file | Path to the output file (table containing transcription factor gathered by gene's pattern groups). | work_directory/Relations_tables/grouped_pattern_relations.csv |
| output_count_table | Path to the new genes per genes' profiles count table. | work_directory/genes_per_gene_pattern_count_tables/grouped_pattern_genes_per_gene_pattern_count.csv |
| max_genes_per_pattern_number | The maximum number of gene per gene's pattern authorizted to allow group with the opposed pattern. | 150 |
| delete_small_gene_patterns | Deletes instead of group gene's patterns with its opposed. Give the gene count threshold. | 15 |


![group_pattern_diagram](group_pattern_diagram.png)