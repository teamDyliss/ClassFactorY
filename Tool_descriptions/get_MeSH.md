### get_MeSH :


#### General description :

This module guides MeSH terms choice for get_annot_tf queries. 
From a set of articles (allows the user to use the litterature about the subject of interest, pubmed) or of TFs, 
the module produces a table of MeSH terms found in this set and the count of articles per term. If a MeSH term is more represented, 
it can be one that well represent the biological context.
This set can also be all papers annotating transcription factors selected. 

Comment : This module require stable internet connection in order to query databases.

#### Arguments : 

| Arguments | Explanations | Examples |
| --- | --- | --- |
| follow_process_path | Path to file storing following process data (.md) | /home/user/workdirectory/Account.md |
| pmid_interest_file_path | Path to the file containing the list of pmid of interest (ex : 27191703) in a no-label column, can be  the pmid or the iri that contain it (ex : https://pubmed.ncbi.nlm.nih.gov/27191703/, select uri)) | work_directory/Results_analysis/pmid_interest.csv |
| output_mesh_file | Path to the file containing the table of mesh count (.csv). | work_directory/Results_analysis/MeSH_terms/my_mesh_file.csv |
| TF_table_MeSH | Path to the file containing the table of transcription factors (.csv). | work_directory/TF_tables/interest_TF_list.csv |
| max_pmid_per_TF_nb | Maximum number of papers annotating a transcription factor (5 to 500). | 50 |
| is_uri | Bool, True if list of pmid given is a uri containing pmid list, as https://pubmed.ncbi.nlm.nih.gov/12524387/.| X |
| email | email address to informs who launch query to nbci (mandatory). | firstname.name@example.fr |
| sep_table | String that separates columns in tables | '\t' |

![get_MeSH_diagram](get_MeSH_diagram.png)
