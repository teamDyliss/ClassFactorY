#! /usr/bin/env python
# -*- coding: utf-8 -*-


'''
@author : Ève Barré

--> General description : 

Script get_pattern_graph_TF.py stores the graph figure (.png) of pattern's targeted by a given transcription factor.
Relation are about if gene's patterns are similar or quite opposed (+/- 1 authorized for one digit of the pattern).

direct opposed edge = red
undirect opposed edge = orange
similar edge = green 
positive regulation = "+"
negative regulation = "-"

TF's table given in input must have a columns of genes' pattern targeted.  

For more parameters : https://networkx.org/documentation/networkx-1.0/reference/generated/networkx.draw.html#networkx.draw 

--> Arguments : 

    "-i", "--input_file" : Path to the file containing transcroption factors' information.
    "-tf", "--Transcription_Factors" : Transcription factor for the one we want the pattern targeted graph.
    "-o", "--output_dir" : Path to the graph's file.

--> Proceeding : 

    - imports transcription factors table (-i)
    - for each TF given (-tf) :
        - select TF's gene's patterns targeted
        - adds regulatory direction to the gene pattern as a node label 
        - creates arcs between two gene patterns regulated if their are opposed (red), quite opposed (orange), or similar (green)
        - nodes positions (x, y) depends on the type or relation (opposed (same y) / similar (same x))
        - stores TF's graph in a .png file (-o)


--> A command line to launch can be like : 
python get_gene_pattern_per_TF_graph.py -i TF_tables\TF_table_q75.csv -tf PRDM1 
'''


import pandas as pd
import argparse 
import time 
import datetime 

import matplotlib.pyplot as plt
import networkx as nx
import os 
from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage
import pipeline

def add_pos_neg(pattern:str, Pattern_targeted_pos:{}) -> str :
    '''
    '''
    if pattern in str(Pattern_targeted_pos.values[0]).split(", ") :
        return pattern + "+"
    return pattern + "-"


def get_gene_pattern_per_TF_graph(max_digit, min_digit, max_distance, follow_process_path, input_file, Transcription_Factors, output_dir, sep_table) :
    '''
    '''

    max_digit = int(max_digit)
    min_digit = int(min_digit)
    max_distance = int(max_distance)

    t0 = time.time()

    # Checks existence of input files : 
    if not os.path.exists(input_file):
        raise ValueError(f"{input_file} does not exist. Change input.")

    if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
        os.makedirs("/".join(follow_process_path.split("/")[:-1]))

    # asserts is a .md file 
    if follow_process_path[-3:] != ".md" :
        raise ValueError(f"'follow_process_path' must be a markdown (.md) file not {follow_process_path.split('.')[-1]}.")

    if max_digit <= min_digit : 
        raise ValueError(f"Minimum pattern's digit {min_digit} must be smaller than the maximum {max_digit}.")
    
    if ((max_digit > 9) | (min_digit > 9)) :
        raise ValueError(f"Patterns' digit must be digit (0 to 9 only).")


    # markdown file to follow process :
    text_info = (f"""

# <font color=orange>Module get_gene_pattern_per_TF_graph.py :</font> 

lauched at {datetime.datetime.today()}

with arguments : 

| Arguments | |
| --- | --- |
| max_digit | {max_digit} |
| min_digit | {min_digit} |
| max_distance | {max_distance} |
| follow_process_path | {follow_process_path} |
| input_file | {input_file} |
| Transcription_Factors | {Transcription_Factors} |
| output_dir | {output_dir} |
| sep_table | {sep_table} |

""")
    print(f"""

********************************************************************************
--> Module get_gene_pattern_per_TF_graph.py :
lauched at {datetime.datetime.today()}
********************************************************************************

""")

    with open(follow_process_path, "a") as info_file:
        # Informations on the srcipt :
        info_file.write(text_info)

    TF_table = pd.read_csv(input_file, sep = sep_table, header = 0)

    for TF in Transcription_Factors.split(", ") :

        # Select data on the transcription factor given : 

        row_TF = TF_table[TF_table["Transcription_Factor"] == TF].reset_index()
        if row_TF.shape[0] == 0 :
            print(f"The transcription factor {TF} is not in the list of selected TF provided.")
            continue 
        
        print(f"Search the graph for the transcription factor : {TF}")

        # Gather all patterns 

        if str(row_TF["Patterns_targeted_neg"].values[0]) == "nan" :
            pattern_list = str(row_TF["Patterns_targeted_pos"].values[0]).split(", ")
        elif str(row_TF["Patterns_targeted_pos"].values[0]) == "nan":
            pattern_list = str(row_TF["Patterns_targeted_neg"].values[0]).split(", ")
        else : 
            pattern_list = (row_TF["Patterns_targeted_pos"] + ", " + row_TF["Patterns_targeted_neg"]).values[0].split(", ")


        # Adds info on regulatory direction : 

        pattern_reg = []
        for c_pattern in pattern_list : 
            pattern_reg.append(add_pos_neg(c_pattern, row_TF["Patterns_targeted_pos"]))
            # add_pos_neg(c_pattern)[:-1]

        pattern_list = pattern_reg
        pattern_reg = []

        # Creates graph :

        G = nx.Graph()

        i_len = 1  # to care with redundancy 

        G.add_node(pattern_list[0])

        edge_colors = []
        nodes_colors = []
        pattern_TF = str(row_TF['Pattern_TF'].values[0])

        if max_distance > (len(pattern_TF) * (max_digit - min_digit)) :
            raise ValueError(f"The following equation is not correct (max_distance > (len(pattern_TF) * (max_digit - min_digit))): {max_distance} > (len({pattern_TF}) * ({max_digit} - {min_digit})).")

        for pattern_1 in pattern_list :
            for pattern_2 in pattern_list[i_len:] :  # to compare just one time all patterns

                if i_len == 1 : 
                    G.add_node(pattern_2)

                (is_opp, Dist) = pipeline.is_similar(pattern_1[:-1], pipeline.get_opposed(pattern_2[:-1], max_digit, min_digit), max_distance)

                if is_opp :
                    G.add_edge(pattern_1, pattern_2)
                    if Dist :
                        edge_colors.append("red")
                        # G[pattern_1][pattern_2]['color'] = 'red'
                    else : 
                        edge_colors.append("orange")
                        # G[pattern_1][pattern_2]['color'] = 'orange'
                elif pipeline.is_similar(pattern_1[:-1], pattern_2[:-1], max_distance)[0] :
                    G.add_edge(pattern_1, pattern_2)
                    edge_colors.append("green")
                    # G[pattern_1][pattern_2]['color'] = 'green'

            i_len += 1

        # To decide nodes coordinates in funciton of if it is opposit or similar to the TF's pattern.
        y_neg = 25
        y_pos = 25
        pos = {}

        for node in G.nodes() :
            (is_opp, Dist) = pipeline.is_similar(node[:-1], pipeline.get_opposed(pattern_TF, max_digit, min_digit), max_distance)
            if Dist == 0 :
                nodes_colors.append("red")
                pos[node] = (25, y_neg)
                y_neg += 1
            elif is_opp :
                nodes_colors.append("orange")
                pos[node] = (25, y_neg)
                y_neg += 1
            elif pipeline.is_similar(node[:-1], pattern_TF[:-1], max_distance)[0] :
                nodes_colors.append("green")
                pos[node] = (20, y_pos)
                y_pos += 1
            else : 
                nodes_colors.append("grey")
                if node[-1] == "-" :
                    pos[node] = (25, y_neg)
                    y_neg += 1
                else : 
                    pos[node] = (20, y_pos)
                    y_pos += 1              



        plt.title(f"TF : {TF}_{pattern_TF}")
        nx.draw(G, pos = pos, with_labels = True, edge_color = edge_colors, node_color = nodes_colors)  # ,  pos =, width = )

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        # to save in a file : 
        if output_dir : 
            plt.savefig(f"{output_dir}/{TF}_graph.png")
            plt.close()


    # Execution time 
    print(f"Ends at {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")
    text_info = f"\nExecution time : {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes"

    with open(follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text_info)
    
    # execution_time (sec), execution_time (min), memory_usage (MiB), input_file_size (MiB)
    return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(os.path.getsize(input_file) / 1024)]

    
if __name__ == "__main__" : 

    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input_file", help = "Path to the file containing transcroption factors' information.")
    parser.add_argument("-tf", "--Transcription_Factors", help = "Transcription factor for the one we want the pattern targeted graph.")
    parser.add_argument("-o", "--output_dir", help = "Path to the graph's file.")
    parser.add_argument("-dir", "--follow_process_path", help = "Path to the directory to save result files.")
    parser.add_argument("--max_distance", help = "Maximum distance between two pattern considered as similar.")
    parser.add_argument("--min_digit", help = "The minimum digit authorized in patterns.")
    parser.add_argument("--max_digit", help = "The maximum digit authorized in patterns.")
    parser.add_argument("-s", "--sep_table", help = "String that separates columns in tables.")

    args = parser.parse_args()

    get_gene_pattern_per_TF_graph(args.max_digit, args.min_digit, args.max_distance, args.follow_process_path, args.input_file, args.Transcription_Factors, args.output_dir, args.sep_table)


