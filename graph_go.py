#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author : Ève Barré

--> General description : 

This script provides a hierargical graph of a Gene Ontology term given (parents and children) in order to
help the choice of GO term in get_annot_tf (to know if TFs are annotated by).
GO terms identifiers must be given, a list is possible (separates by a coma), as : 0002317, 0019724.

This module require internet connection to query databases.

--> Arguments : 

"-go", "--go_terms" : Label and gene ontology identifer for the one the parent graph is wanted (example : 0002317, 0019724).
"-o", "--output_file" : Path to the graph's file. (.png)
"--follow_process_path" : Path to the file to save process data (markdown).

--> Proceeding :

  - search the gene ontology terms graph in <https://www.ebi.ac.uk/QuickGO/> database containing all go terms (-go) in input
  - stores this grpah in a image .png (-o)
  - writes in a file process data : 
    - module name
    - query data and time 
    - arguments 

--> In pipeline : 

graph_go can be used before get_annot_tf to help Go term choice in input. 

--> Usage tips : 

The hierarchical graph allow to select a GO term that is more general but that still 
fits to the biological context of interest. For example : 'lymphocyte activation' is more general 
but still correspond to our biological context of plasma cell differentiation (which concerns more
TFs than 'B-lymphocyte' which is more specific). 
Get_annot_tf permits the use of several GO terms, therefore, more specific and more general terms could both be used.
Then, a more precise TF's classification can be done. 

--> Execution time and memory evolution : 

Execution time is very fast. 
'''

import requests, sys
import pandas as pd
import argparse 
import time 
import datetime 
import matplotlib.pyplot as plt
import os 
from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage

def graph_go(follow_process_path, go_terms, output_file) :
  '''
  '''

  t0 = time.time()

  # asserts is a .md file 
  if follow_process_path[-3:] != ".md" :
    raise ValueError(f"'follow_process_path' must be a markdown (.md) file not {follow_process_path.split('.')[-1]}.")

  if not all([go.isdigit() for go in go_terms.split(', ')]) :
    raise ValueError(f"Go terms must be a list of interger as '0002317, 0019724'.")

  if any([(len(go) != 7) for go in go_terms.split(', ')]) :
    raise ValueError(f"go terms must have 7 digits.")
    
  # markdown file to follow process :
  text_info = (f"""

# <font color=orange>Module graph_go.py :</font> 

lauched at {datetime.datetime.today()}

with arguments : 

| Arguments | |
| --- | --- |
| go_terms | {go_terms} |
| follow_process_path | {follow_process_path} |
| output_file | {output_file} |

""")
  print(f"""

********************************************************************************
--> Module graph_go.py :
lauched at {datetime.datetime.today()}
********************************************************************************

""")

  if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
      os.makedirs("/".join(follow_process_path.split("/")[:-1]))

  with open(follow_process_path, "a") as follow_process_file :
      follow_process_file.write(text_info)


  go_terms = go_terms.split(", ")
  all_go_terms = go_terms[0]

  # Write the correct pieces of the query with go terms of intetrest
  for c_go in go_terms[1:] :
      all_go_terms += "%2CGO%3A" + c_go

  # Image parameters could be modified if necessary while adding a argument for it 
  requestURL = "https://www.ebi.ac.uk/QuickGO/services/ontology/go/terms/{ids}/chart?ids=GO%3A" + all_go_terms + "&base64=false&showKey=true&showIds=true&termBoxWidth=80&termBoxHeight=50&showChildren=true&fontSize=10"

  r = requests.get(requestURL, headers={ "Accept" : "image/png"})

  if not r.ok:
    r.raise_for_status()
    sys.exit()

  if not os.path.exists("/".join(output_file.split("/")[:-1])):
    os.makedirs("/".join(output_file.split("/")[:-1]))

  with open(output_file, "wb") as imgFile : 
      imgFile.write(r.content)

  # Execution time 
  print(f"Ends at {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")
  
  if "/".join(follow_process_path.split("/")[:-1]) in output_file : 
    graph_path = output_file.split("/".join(follow_process_path.split("/")[:-1]))[1][1:]
  else : 
    graph_path = ("/".join(follow_process_path.split("/")[:-1]) + "graph_go.png")
    with open(graph_path, "wb") as imgFile : 
      imgFile.write(r.content)
    

  text_info = f"""


## Gene Ontology terms graph : 


![graph_go]({graph_path})


Execution time : {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes

"""
  with open(follow_process_path, "a") as follow_process_file :
      follow_process_file.write(text_info)
  
  # execution_time (sec), execution_time (min), memory_usage (MiB), input_file_size (MiB)
  return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), 0]

if __name__ == "__main__" : 

  parser = argparse.ArgumentParser()

  parser.add_argument("-go", "--go_terms", help = "Label and gene ontology identifer for the one the parent graph is wanted.")
  parser.add_argument("--follow_process_path", help = "Path to the file to save process data.")
  parser.add_argument("-o", "--output_file", help = "Path to the graph's file.")

  args = parser.parse_args()

  graph_go(args.follow_process_path, args.go_terms, args.output_file)