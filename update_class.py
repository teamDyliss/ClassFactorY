#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author : Ève Barré

--> General description :

In order to follow TF selection, update_class (launched between each filtering module) classifies TF in a table 
where each line is a filter. It enables to check through which filter a TF of interest is lost for instance.
Filters could be : 
    - all TF
    - differential expression +/- constant
    - compatibility table 
    - specificity + coverage
    - ? : annotations, publication's quotation number ... 

New modifications : deletes parts conserning annotations evolutions because score have been created later and are a better way to assess.

--> Arguments : 

    "-t", "--TF_table" : Path to the new file containing Transcription factors that passed filter, must have column label 'Transcription_Factor'.
    "-n", "--filter_name" : Filter just applied.
    "-ct", "--class_table_path" : Path to output file containing the classification table if an ohter filter has already been done (markdown file : .md).
    "--go_terms_as_column" : Go terms to keep as column in the classification (gives the annotated TF count).
    "--quotations_count_as_column" : Papers quotation count ('G_quotations_nb', 'strict_quotations_nb', 'large_quotations_nb' to keep as column in the classification (gives the annoated TF count).
    "--annotation_table" : All TF annotations table path (.csv file).
    "--follow_process_path" : Path to the file to save process data (markdown, .md).
    "-s", "--sep_table" : String that separates columns in tables, as '\t' or ','.

--> Proceeding : 

    - writes in a file data on the process (--follow_process_path) : 
        - module's name 
        - date and time of query
        - arguments used
        - execution time and memory usage
    - imports TF table
    - if already exists, imports class_table
    - compares which TF are and aren't passed the previous filter and counts
    - if annotation table given : merges anntations informations for new line and previous line modified
    - stores class_table updated
    
--> In pipeline : 

Class_TF is launch between each module making a TF selection. 
The class table is writting at the end of the file following the process. 

--> Usage tips : 

Class talbe must be very usful to follow TF through filters and selection.
It can be used to asess parameters used and see if TF of interest are too early or too lately selected. 
The classification could allows to apply many filters and see when TF are lost.
Additional TF annotations counts on the classification table permits to evaluate if 
filter select well annotated TF for a specific context or not. 

'''

import pandas as pd
import argparse 
import time 
import datetime 
import os
import matplotlib.pyplot as plt
from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage
import ast 

# functions needed : 

def get_annotated_row(list_str_TF, df_TF, annotation_table, filtered_TF_nb:int) :  # go_terms_as_column, quotations_count_as_column,
    '''
    '''

    list_str_TF = list_str_TF.split(", ")

    col_annot = ["Transcription_Factor"]

    # if go_terms_as_column : 
    #     go_terms = []
    #     for go_term in go_terms_as_column.split(", ") :
    #         go_terms.append([key for key in annotation_table.columns if go_term in key][0])
    #     col_annot += go_terms

    # if quotations_count_as_column : 
    #     quotations = []
    #     for quotation in quotations_count_as_column.split(", ") : 
    #         quotations.append(quotation)
    #     col_annot += quotations


    TF_annotated_table = pd.DataFrame({"Transcription_Factor" : list_str_TF})
    TF_annotated_table = TF_annotated_table.merge(annotation_table[col_annot].drop_duplicates(), how = "left")

    # if go_terms_as_column : 
    #     for go_term in go_terms_as_column.split(", ") :
    #         df_TF[f"{[key for key in annotation_table.columns if go_term in key][0]}_%"] = round(((list(TF_annotated_table[[key for key in annotation_table.columns if go_term in key][0]]).count("X")) / filtered_TF_nb) * 100)
        
    # if quotations_count_as_column : 
    #     for quotation in quotations_count_as_column.split(", ") : 
    #         df_TF[quotation] = TF_annotated_table[quotation].mean()
        
    return df_TF


def update_class(follow_process_path, TF_table, filter_name, class_table_path, annotation_table, sep_table) :  # , go_terms_as_column, quotations_count_as_column) :
    '''
    '''

    t0 = time.time()

    # asserts is a .md file 
    if follow_process_path[-3:] != ".md" :
        raise ValueError(f"'follow_process_path' must be a markdown (.md) file not {follow_process_path.split('.')[-1]}.")

    print(f"""

********************************************************************************
--> Module update_class.py :
lauched at {datetime.datetime.today()}
********************************************************************************

""")

    # Checks input files' existance :
    if not os.path.exists(TF_table):
        raise ValueError(f"{TF_table} does not exist. Change input.")
    
    if not os.path.exists(annotation_table):
        raise ValueError(f"{annotation_table} does not exist. Change input.")

    if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
        os.makedirs("/".join(follow_process_path.split("/")[:-1]))
    
    if not os.path.exists("/".join(class_table_path.split("/")[:-1])):
        os.makedirs("/".join(class_table_path.split("/")[:-1]))

    # markdown file to follow process :
    text_info = (f"""

# <font color=orange>Module update_class.py :</font> 

lauched at {datetime.datetime.today()}

with arguments : 

| Arguments | |
| --- | --- |
| filter_name | {filter_name} |
| TF_table | {TF_table} |
| class_table | {class_table_path} |
| follow_process_path | {follow_process_path} |
| annotation_table | {annotation_table} |
| sep_table | {sep_table} |


""")

# | go_terms_as_column | {go_terms_as_column} |
# | quotations_count_as_column | {quotations_count_as_column} |

    with open(follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text_info)
    
    input_size = os.path.getsize(TF_table) 
    
    # Imports data : 
    TF_table = pd.read_csv(TF_table, sep = sep_table, header = 0, usecols = ["Transcription_Factor"]).drop_duplicates()

    # if type(dict_annot_columns_id) == str : 
    #     dict_annot_columns_id = ast.literal_eval(dict_annot_columns_id)

    if os.path.exists(class_table_path) : 
        input_size += os.path.getsize(class_table_path) 

        class_table =  pd.read_csv(class_table_path, sep = sep_table, header = 0)

        # Defines with TF just failed to pass filter 

        new_previous_TF_list = ""
        next_TF_list = ""

        for old_TF in class_table["filtered_TF"][0].split(", ") :
            if old_TF in list(TF_table["Transcription_Factor"]) :
                if len(next_TF_list) == 0 : 
                    next_TF_list += old_TF
                else : 
                    next_TF_list += ", " + old_TF
            else : 
                if len(new_previous_TF_list) == 0 :
                    new_previous_TF_list += old_TF
                else  :
                    new_previous_TF_list += ", " + old_TF
        
        previous_row = pd.DataFrame({"filter_name":[class_table["filter_name"][0]], "filtered_TF" : [new_previous_TF_list], "filtered_TF_nb" : [len(new_previous_TF_list.split(', '))]})

    else : 
        next_TF_list = ", ".join(list(TF_table["Transcription_Factor"]))
        new_previous_TF_list = None
    

    new_row = pd.DataFrame({"filter_name":[filter_name], "filtered_TF" : [next_TF_list], "filtered_TF_nb" : [len(next_TF_list.split(', '))]})


    if annotation_table : 
        annotation_table = pd.read_csv(annotation_table, sep = sep_table, header = 0)

        new_row = get_annotated_row(next_TF_list, new_row, annotation_table, len(next_TF_list.split(', ')))  # go_terms_as_column, quotations_count_as_column,

        if os.path.exists(class_table_path) : 
                previous_row = get_annotated_row(new_previous_TF_list, previous_row, annotation_table, class_table["filtered_TF_nb"][0])  # go_terms_as_column, quotations_count_as_column,


    # modifiying class_table : 

    if not os.path.exists(class_table_path) : 
        class_table = new_row
        new_row.to_csv(class_table_path, sep = sep_table, index = None)

    else : 
        # Replace TF list modified : 
        class_table.loc[0] = previous_row.loc[0]  # rather just modify the TF_filtered number but not annotations on it 
        new_row.append(class_table, ignore_index = True).to_csv(class_table_path, sep = sep_table,  index = None)  # Rather but rows in the other order ?


    # Execution time 
    print(f"Ends at {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")
    text_info = f"\nExecution time : {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes"

    with open(follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text_info)
    

    # execution_time (sec), execution_time (min), memory_usage (MiB), input_file_size (MiB)
    return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(input_size / 1024)]



if __name__ == "__main__" : 

    parser = argparse.ArgumentParser()

    parser.add_argument("-t", "--TF_table", help = "Path to the new file containing Transcription factors that passed filter.")
    parser.add_argument("-n", "--filter_name", help = "Filter just applied.")
    parser.add_argument("-ct", "--class_table_path", help = "Path to output file containing the classification table if an ohter filter has already been done.")
    # parser.add_argument("--go_terms_as_column", help = "Go terms to keep as column in the classification (gives the annotated TF count).")
    # parser.add_argument("--quotations_count_as_column", help = "Papers quotation count ('G_quotations_nb', 'strict_quotations_nb', 'large_quotations_nb' to keep as column in the classification (gives the annoated TF count).")
    parser.add_argument("--annotation_table", help = "All TF annotations table path.")
    parser.add_argument("--follow_process_path", help = "Path to the file to save process data.")
    parser.add_argument("-s", "--sep_table", help = "String that separates columns in tables.")

    args = parser.parse_args()

    update_class(args.follow_process_path, args.TF_table, args.filter_name, args.class_table_path, args.annotation_table, args.sep_table)  # , args.go_terms_as_column, args.quotations_count_as_column)