#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author : Ève Barré

--> General description : 
This module class variants if they are located on a transcription factor given binding sites (TFBS) on target gene's regulatory regions.
If data are not given, TFBS can be obtained on JASPAR REST API and gene coordinates in Ensembl REST API. 

VCF file must have #CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO first line. 

--> Arguments : 

  "--reference_genome" : Reference genome used and to use to search genome coordinates.
  "-s", "--sep_table" : String that separates columns in transcription table of interest.
  "--JASPAR_version" : If transcription factor binding sites must be search, precise which version of JASPAR database use (2020, 2018...)
  "--TF_gene_region_file" : Relations_table path, can contain or not regions coordinates.
  "--VCF_file" : Path to the Variant Calling Format file.
  "--output_variants_file" : Path to the file to output (with variants per transcription factor found).
  "--regulatory_gene_distance" : Distance between a gene and its regulatory regions to consider (ex : 500000 bases).
  "--are_regulatory_region_given" : Bool, if regions' genomic coordinates are already given in the relation table (start, end, chrom).
  "--TFBS_file" : Path to the transcription binding sites file if already search, otherwise, search on JASPAR.
  "--follow_process_path" : Path to the file to save process data.
  "--is_regulatory_direction_given" : Bool, if regulatory direction is given as a column in relations table.

--> Proceeding : 

- If regulatory region coordinates are not given (start, end, chrom) (--are_regulatory_region_given): 
    - searchs for each genes its genomes coordinates in ensembl database (for human genome) 
    through REST API for a reference genome given (--reference_genome)
    - defines regulatory regions at a given distance to genes location (--regulatory_gene_distance) 
- Selects variants (--VCF_file) that are in a regulatory region defined
- if TFBS are not given (TFBS_file), search bindings sites for each selected TF in JASPAR database 
through a REST API for a given version (--JASPAR_version) for the same reference genome
- Selects variants which are in a transcription factor binding site (TFBS): 
- Provide as output columns : 
    - 1) TF, 
    - 2) bases number of regulatory region of genes targeted by the TF, 
    - 3) number of variants in regulatory region of targeted genes, 
    - 4) mutational charge (3/2)
    - 5) Variants with its data
- writes in a file (follow_process_path) following informations  on query : 
    - Module name 
    - Date and time of launch
    - Arguments 

--> In pipeline : 

Select_variants is launch in the second part as a additional annotation. It can be used independently to other modules 
in order to prioritize variants but also with others to prioritize transcription factors. 

--> Usage tips : 

--> Performance : 

'''


import requests, sys
import ast 
import coreapi
import argparse
import pandas as pd
import time
import datetime
import io
import os
import matplotlib.pyplot as plt 
from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage
import ast
import pipeline
# import bedtools
# import pybedtools

"""
a = pybedtools.BedTool('mydata/snps.bed')
a.intersect('mydata/exons.bed').saveas('snps-in-exons.bed', trackline="track name='SNPs in exons' color=128,0,0")
"""

def select_variants(region_coordinates:str, TF_gene_region_file, TF_gene_file, Variants_coordinates, output_variants_file, follow_process_path, sep_table, TFBS_file:str, is_regulatory_direction_given:bool) : 
  '''
  '''

  t0 = time.time()

  # asserts is a .md file 
  if follow_process_path[-3:] != ".md" :
      raise ValueError(f"'follow_process_path' must be a markdown (.md) file not {follow_process_path.split('.')[-1]}.")
  
  # Checks existence of input files : 
  if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
    os.makedirs("/".join(follow_process_path.split("/")[:-1]))

  follow_process_dir = "/".join(follow_process_path.split("/")[:-1])

  if not os.path.exists(TF_gene_region_file) :
      raise ValueError(f"{TF_gene_region_file} does not exist. Change input.")

  if not os.path.exists(region_coordinates) :
      raise ValueError(f"{region_coordinates} does not exist. Change input.")

  if TF_gene_file : 
    if not os.path.exists(TF_gene_file) :
      raise ValueError(f"{TF_gene_file} does not exist. Change input.")

  if not os.path.exists(Variants_coordinates):
      raise ValueError(f"{Variants_coordinates} does not exist. Change input.")

  if not os.path.exists(TFBS_file):
      raise ValueError(f"{TFBS_file} does not exist. Change input.")
  
  if not os.path.exists("/".join(output_variants_file.split("/")[:-1])):
    os.makedirs("/".join(output_variants_file.split("/")[:-1]))

  input_file_size = os.path.getsize(TF_gene_region_file) + os.path.getsize(TF_gene_region_file) + os.path.getsize(Variants_coordinates) + os.path.getsize(TFBS_file)

  # markdown file to follow process :
  text_info = (f"""

# <font color=orange>Module Select_variants.py :</font> 

lauched at {datetime.datetime.today()}

with arguments : 

| Arguments | |
| --- | --- |
| TF_gene_region_file | {TF_gene_region_file} |
| TF_gene_file | {TF_gene_file} |
| region_coordinates | {region_coordinates} |
| output_variants_file | {output_variants_file} |
| sep_table | {sep_table} |
| Variants_coordinates | {Variants_coordinates} |
| TFBS_file | {TFBS_file} |
| follow_process_path | {follow_process_path} |
| is_regulatory_direction_given | {is_regulatory_direction_given} |  


""")

  print(f"""

********************************************************************************
--> Module Select_variants.py :
lauched at {datetime.datetime.today()}
********************************************************************************

""")

  if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
      os.makedirs("/".join(follow_process_path.split("/")[:-1]))

  with open(follow_process_path, "a") as info_file:
      # Informations on the srcipt :
      info_file.write(text_info)

  # 1) Import relations files and merge if necessary with regions : 

  if is_regulatory_direction_given : 
    TF_gene_region_table = pd.read_csv(TF_gene_region_file, sep = sep_table, header = 0, engine = "python", usecols = ["Transcription_Factor", "Gene", "Reg", "ID_ATAC"]).drop_duplicates()
  else : 
    TF_gene_region_table = pd.read_csv(TF_gene_region_file, sep = sep_table, header = 0, engine = "python", usecols = ["Transcription_Factor", "Gene", "ID_ATAC"]).drop_duplicates()

  if TF_gene_file :  # merge with selected TF-gene relations 
    TF_gene_region_table = TF_gene_region_table.merge(pd.read_csv(TF_gene_file, sep = sep_table, header = 0, engine = "python", usecols = ["Transcription_Factor", "Gene"]).drop_duplicates(), how = "right")

  # keep the list of TF-gene relations in order to merge at the end on the analysis and selection of non coding variants  : 
  TF_gene_table = TF_gene_region_table[["Transcription_Factor", "Gene"]].drop_duplicates()

  # merge with region coordinates
  TF_gene_region_table = pd.read_csv(region_coordinates, sep = sep_table, header = 0, engine = "python", usecols = ["chrom", "start", "end", "ID_ATAC"]).drop_duplicates().merge(TF_gene_region_table, how = "left")

  # 2) calculates accessibles base number per TF for all initial TF in order to make a mutation proportion with the variants in gene regulatory region number

  TF_gene_region_table["base_nb"] = TF_gene_region_table["end"].astype(int) - TF_gene_region_table["start"].astype(int)
  TF_gene_region_table = TF_gene_region_table.merge(TF_gene_region_table[["Transcription_Factor", "base_nb"]].groupby(["Transcription_Factor"])["base_nb"].sum().reset_index(), how = "inner")

  # Store TF_gene_region_table to use bedtools with the path of output in order to replace it with the output table
  TF_gene_region_table.to_csv(output_variants_file, sep = sep_table, index = None) # Named outut_variants_file but just to have a temporary file, not to use another file to delete 

  # intersect variants and region (named temporary 'output_variants_file') coordinates

  os.system(f"bedtools intersect -a {output_variants_file} -b {Variants_coordinates} >> {output_variants_file}")  
  print("First bedtools command achieved")

  TF_gene_region_table = pd.read_csv(output_variants_file, sep = sep_table, header = 0)  # Re read the temporary file of storage 

  print(TF_gene_region_table.head())

  # Count the number of variants in TF's regualtory region and mutational charge
  TF_gene_region_table = TF_gene_region_table.merge(TF_gene_region_table[["Transcription_Factor", "Gene", "end"]].groupby(["Transcription_Factor", "Gene"])["end"].count().rename("Variants_in_region_nb").reset_index(), how = "left")
  TF_gene_region_table["mutational_charge_per_Mb"] = ( TF_gene_region_table["Variants_in_region_nb"] / TF_gene_region_table["base_nb"] ) * 100000

  TF_gene_region_table.to_csv(output_variants_file, index = None, sep = sep_table) # Named outut_variants_file but just to have a temporary file, not to use another file to delete 

  # 3) Select variants that are in TFBS  # or with subprocess.call
  os.system(f"bedtools intersect -a {TF_gene_region_table} -b {TFBS_file} >> {output_variants_file}")  
  print("Second bedtools command achieved")

  TF_gene_region_table = pd.read_csv(output_variants_file, sep = sep_table, header = 0)  # Re read the temporary file of storage

  TF_gene_region_table = TF_gene_region_table.merge(TF_gene_region_table[["Transcription_Factor", "Gene", "end"]].groupby(["Transcription_Factor", "Gene"])["end"].count().rename("Variants_in_TFBS_nb").reset_index(), how = "left")
 
  # merge with all initial TF
  # TF_gene_region_table = TF_gene_region_table.merge(TF_gene_table, how = "outer")

  TF_gene_region_table.to_csv(output_variants_file, index = None, sep = sep_table)

  print("Stores statistical information")

  pipeline.stat(TF_gene_region_table[["mutational_charge_per_Mb"]], "mutational_charge_per_Mb", "hist, box", sep_table, f"{follow_process_dir}/graphics")
  pipeline.stat(TF_gene_region_table[["Variants_in_region_nb"]], "Variants_in_region_nb", "hist, box", sep_table, f"{follow_process_dir}/graphics")
  pipeline.stat(TF_gene_region_table[["Variants_in_TFBS_nb"]], "Variants_in_TFBS_nb", "hist, box", sep_table, f"{follow_process_dir}/graphics")
  pipeline.stat(TF_gene_region_table[["base_nb"]], "base_nb", "hist, box", sep_table, f"{follow_process_dir}/graphics")


  text_info += f"""


## mutational_charge_per_Mb : 
***

{pipeline.display_table_stat("graphics/mutational_charge_per_Mb_hist.png", "mutational_charge_per_Mb_hist", "graphics/mutational_charge_per_Mb_box.png" , "mutational_charge_per_Mb_box", pipeline.makes_percentiles_intervals(TF_gene_region_table[["Transcription_Factor", "mutational_charge_per_Mb"]], "mutational_charge_per_Mb").to_markdown())}

***
"""

  text_info += f"""


## Variants_in_region_nb : 
***

{pipeline.display_table_stat("graphics/Variants_in_region_nb_hist.png", "Variants_in_region_nb_hist", "graphics/Variants_in_region_nb_box.png" , "Variants_in_region_nb_box", pipeline.makes_percentiles_intervals(TF_gene_region_table[["Transcription_Factor", "Variants_in_region_nb"]], "Variants_in_region_nb").to_markdown())}

***
"""

  text_info += f"""


## Variants_in_TFBS_nb : 
***

{pipeline.display_table_stat("graphics/Variants_in_TFBS_nb_hist.png", "Variants_in_TFBS_nb_hist", "graphics/Variants_in_TFBS_nb_box.png" , "Variants_in_TFBS_nb_box", pipeline.makes_percentiles_intervals(TF_gene_region_table[["Transcription_Factor", "Variants_in_TFBS_nb"]], "Variants_in_TFBS_nb").to_markdown())}

***
"""

  text_info += f"""


## base_nb : 
***

{pipeline.display_table_stat("graphics/base_nb_hist.png", "base_nb_hist", "graphics/base_nb_box.png" , "base_nb_box", pipeline.makes_percentiles_intervals(TF_gene_region_table[["Transcription_Factor", "base_nb"]], "base_nb").to_markdown())}

***
"""


  # Execution time 
  print(f"Ends at {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")
  text_info += f"\nExecution time : {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes"

  with open(follow_process_path, "a") as follow_process_file :
      follow_process_file.write(text_info)
  # execution_time (sec), execution_time (min), memory_usage (MiB), relations_table_size (MiB)

  return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(input_file_size / 1024)]

if __name__ == "__main__" : 

    parser = argparse.ArgumentParser()

    parser.add_argument("-s", "--sep_table", help = "String that separates columns in transcription table of interest.")
    parser.add_argument("--TF_gene_region_file", help = "Relations_table path, that contains regions identifier (Transcription_Factor, Gene, ID_ATAC +/- Reg) (Regulus output for instance).")
    parser.add_argument("--TF_gene_file", help = "Relations_table path, that contains TF-gene to keep relations (select_tf output for instance) but not regions.")
    parser.add_argument("--region_coordinates", help = "Path to the coordinates (chrom, start, end, ID_ATAC) of regulatory regions.")
    parser.add_argument("--Variants_coordinates", help = "Path to the variants coordinates (chrom, start, end...)")
    parser.add_argument("--output_variants_file", help = "Path to the file to output (with variants per transcription factor found).")
    parser.add_argument("--TFBS_file", help = "Path to the transcription binding sites file if already search, otherwise, search on JASPAR.")
    parser.add_argument("--follow_process_path", help = "Path to the file to save process data.")
    parser.add_argument("--is_regulatory_direction_given", help = "Bool, if regulatory direction is given as a column in relations table.")
    
    args = parser.parse_args()

    select_variants(args.region_coordinates, args.TF_gene_region_file, args.TF_gene_file, args.Variants_coordinates, args.output_variants_file, args.follow_process_path, args.sep_table, args.TFBS_file, args.is_regulatory_direction_given)

    # 1) sans données, en utilisant les bases de données :
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --reference_genome hg38 --JASPAR_version 2020 --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/tests/remapped_hg38_S1_Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/tests/2_remapped_hg38_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/tests/follow_process.md --sep_table sep_table --regulatory_gene_distance 500000 --is_regulatory_direction_given True
    
    # 2) avec TFBS sans regions
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --reference_genome hg19 --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/tests/S1_Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/tests/hg19_TFBS_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/tests/follow_process.md --sep_table sep_table --regulatory_gene_distance 500000 --is_regulatory_direction_given True --TFBS_file "/home/eveadmin/Bureau/Variants/tests/TFBS_file/tf_loc_sub1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/tf_loc_sub2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/tf_loc_sub3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/tf_loc_sub4.bed"    
    
    # 3) sans TFBS avec regions
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --reference_genome hg38 --JASPAR_version 2020 --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/remapped_region_selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/tests/remapped_hg38_S1_Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/tests/hg38_regions_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/tests/follow_process.md --sep_table sep_table --regulatory_gene_distance 500000 --is_regulatory_direction_given True --are_regulatory_region_given True
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --reference_genome hg38 --JASPAR_version 2020 --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/remapped_region_selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/tests/remapped_hg38_S1_Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/tests/hg38_regions_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/tests/follow_process.md --sep_table sep_table --regulatory_gene_distance 500000 --is_regulatory_direction_given True --are_regulatory_region_given True

    # 4) avec TFBS et regions 
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/coordinates_region_selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/tests/S1_Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/tests/hg19_TFBS_region_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/tests/follow_process.md --sep_table sep_table --regulatory_gene_distance 500000 --is_regulatory_direction_given True --TFBS_file "/home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_6.bed" --are_regulatory_region_given True
    
    # , /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_6.bed
    # , /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_6.bed
    # , /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_6.bed
    # , /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_6.bed

    # , /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_6.bed

    # S1
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/coordinates_region_selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/S1/Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/S1/hg19_TFBS_region_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/S1/follow_process.md --sep_table sep_table --regulatory_gene_distance 500000 --is_regulatory_direction_given True --TFBS_file "/home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_6.bed" --are_regulatory_region_given True

    # S2
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/coordinates_region_selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/S2/Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/S2/hg19_TFBS_region_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/S2/follow_process.md --sep_table sep_table --regulatory_gene_distance 500000 --is_regulatory_direction_given True --TFBS_file "/home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_6.bed" --are_regulatory_region_given True

    # S3
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/coordinates_region_selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/S3/Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/S3/hg19_TFBS_region_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/S3/follow_process.md --sep_table sep_table --regulatory_gene_distance 500000 --is_regulatory_direction_given True --TFBS_file "/home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_6.bed" --are_regulatory_region_given True

    # S4
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/coordinates_region_selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/S4/Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/S4/hg19_TFBS_region_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/S4/follow_process.md --sep_table sep_table --regulatory_gene_distance 500000 --is_regulatory_direction_given True --TFBS_file "/home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_6.bed" --are_regulatory_region_given True

    # S5
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/coordinates_region_selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/S5/Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/S5/hg19_TFBS_region_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/S5/follow_process.md --sep_table sep_table --regulatory_gene_distance 500000 --is_regulatory_direction_given True --TFBS_file "/home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_6.bed" --are_regulatory_region_given True

    # S6
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/coordinates_region_selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/S6/Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/S6/hg19_TFBS_region_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/S6/follow_process.md --sep_table sep_table --regulatory_gene_distance 500000 --is_regulatory_direction_given True --TFBS_file "/home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub1_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub2_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub3_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_file/split_tf_loc/sub4_6.bed" --are_regulatory_region_given True
