#! /usr/bin/env python
# -*- coding: utf-8 -*-


''' 
@author : Ève Barré

--> General description : 

This script provides the graph figure (.png) of TF' patterns selected.
Relation are about if tf's patterns are similar or quite opposed (+/- 1 authorized for one digit of the pattern).

direct opposed edge = red
undirect opposed edge = orange
similar edge = green 
positive regulation = "+"
negative regulation = "-"

See mores : 
Combine informations about which TF have which relation.
In addition, table of specific TF for each relations is provide in a file. 

TF's table given in input must have a columns of genes' pattern targeted.  

For more parameters : https://networkx.org/documentation/networkx-1.0/reference/generated/networkx.draw.html#networkx.draw 

--> Arguments : 

    "-i", "--input_file" : Path to the file containing relations selected.
    "-o", "--output_file" : Path to the graph's file.

--> Proceeding : 

    - imports trascription factors table (-i)
    - gathers TF's pattern and its regulatory drirection as a node label
    - creates an arc between two TFs if ther are opposed or similar (when only one patterns' digit differ from 1)
    colors of this arcs depends on their relation type (opposed (red), quite opposed (orange) or similar (green))
    - for nodes positions in graph : try to opposed two opposed patterns and to put near two similar patterns, 
    give a position in funciton of the first arc defined
    - stores the graph in a file .png (-o)

--> command line to launch can be like : 
python get_TF_pattern_graph.py -i TF_tables/TF_table_q75.csv  
'''


import pandas as pd
import argparse 
import time 
import datetime 

import matplotlib.pyplot as plt
import networkx as nx
import os  
from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage
import pipeline 

def get_TF_pattern_graph(max_digit, min_digit, max_distance, follow_process_path, input_file, output_file, sep_table) :
    '''
    '''

    t0 = time.time()
    max_digit = int(max_digit)
    min_digit = int(min_digit)
    max_distance = int(max_distance)

    # asserts is a .md file 
    if follow_process_path[-3:] != ".md" :
        raise ValueError(f"'follow_process_path' must be a markdown (.md) file not {follow_process_path.split('.')[1]}.")

    if max_digit <= min_digit : 
        raise ValueError(f"Minimum pattern's digit {min_digit} must be smaller than the maximum {max_digit}.")
    
    if ((max_digit > 9) | (min_digit > 9)) :
        raise ValueError(f"Patterns' digit must be digit (0 to 9 only).")

    # markdown file to follow process :
    text_info = (f"""

# <font color=orange>Module get_TF_pattern_graph.py :</font> 

lauched at {datetime.datetime.today()}

with arguments : 

| Arguments | |
| --- | --- |
| max_digit | {max_digit} |
| min_digit | {min_digit} |
| max_distance | {max_distance} |
| follow_process_path | {follow_process_path} |
| input_file | {input_file} |
| output_file | {output_file} |
| sep_table | {sep_table} |

""")
    print(f"""

********************************************************************************
--> Module get_TF_pattern_graph.py :
lauched at {datetime.datetime.today()}
********************************************************************************

""")

    # Checks existence of input files : 
    if not os.path.exists(input_file):
        raise ValueError(f"{input_file} does not exist. Change input.")

    if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
        os.makedirs("/".join(follow_process_path.split("/")[:-1]))

    with open(follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text_info)
    
    TF_table = pd.read_csv(input_file, sep = sep_table, header = 0)
    TF_table = TF_table[["Pattern_TF", "Transcription_Factor"]].drop_duplicates().groupby(["Pattern_TF"])["Transcription_Factor"].agg(", ".join).reset_index(name = "TFs")
    TF_table["Pattern_TF"] = TF_table["Pattern_TF"].astype(str)

    # Creates graph :

    G = nx.Graph()

    i_len = 1  # to care with redundancy 

    tfs_patterns = list(TF_table["Pattern_TF"])
    tfs = list(TF_table["TFs"])

    if max_distance > (len(tfs_patterns[0]) * (max_digit - min_digit)) :
        raise ValueError(f"The following equation is not correct (max_distance > (len(pattern_TF) * (max_digit - min_digit))): {max_distance} > {len(tfs_patterns[0])} * ({max_digit} - {min_digit})).")



    G.add_node(tfs[0])

    edge_colors = []
    pos = {}
    y = 0
    step = 7
    x_1 = 0
    x_2 = 10

    # Adds an edge if patterns are opposes or similar, try with all gene patterns duo.
    for i_1, pattern_1 in enumerate(tfs_patterns) :
        for i_2, pattern_2 in enumerate(tfs_patterns[i_len:]) :  # to compare just one time all patterns
            if i_len == 1 : 
                G.add_node(tfs[i_2])       

            (is_opp, Dist) = pipeline.is_similar(pattern_1, pipeline.get_opposed(pattern_2, max_digit, min_digit), max_distance)         
            if is_opp :
                G.add_edge(tfs[i_1], tfs[i_2]) 

                if (tfs[i_1] not in pos.keys()) & (tfs[i_2] in pos.keys()) :
                    pos[tfs[i_1]] = (abs(pos[tfs[i_2]][0] - x_2), y)
                    y += step

                elif (tfs[i_2] not in pos.keys()) & (tfs[i_1] in pos.keys()) :
                    pos[tfs[i_2]] = (abs(pos[tfs[i_1]][0] - x_2), y)
                    y += step
                
                elif (tfs[i_2] not in pos.keys()) & (tfs[i_1] not in pos.keys()) :
                    pos[tfs[i_1]] = (x_2, y)
                    y += step 

                    pos[tfs[i_2]] = (x_1, y)
                    y += step 
    
                if Dist == 0 :
                    edge_colors.append("red")
                    # G[pattern_1][pattern_2]['color'] = 'red'         
                else : 
                    edge_colors.append("orange")
                    # G[pattern_1][pattern_2]['color'] = 'orange'
            elif pipeline.is_similar(pattern_1, pattern_2, max_distance) :
                G.add_edge(tfs[i_1], tfs[i_2])

                if (tfs[i_1] not in pos.keys()) & (tfs[i_2] in pos.keys()) :
                    pos[tfs[i_1]] = (pos[tfs[i_2]][0], y)
                    y += step

                elif (tfs[i_2] not in pos.keys()) & (tfs[i_1] in pos.keys()) :
                    pos[tfs[i_2]] = (pos[tfs[i_1]][0], y)
                    y += step
                
                elif (tfs[i_2] not in pos.keys()) & (tfs[i_1] not in pos.keys()) :
                    pos[tfs[i_1]] = (x_1, y)
                    y += step 

                    pos[tfs[i_2]] = (x_1, y)
                    y += step 

                edge_colors.append("green")
                # G[pattern_1][pattern_2]['color'] = 'green'

        i_len += 1
            

    # Defines coordinates in function of opposite or similar : 
    # if similar : same x
    # if opposed : separate x



    plt.title(f"TF_pattern_graph")

    nx.draw(G, pos = pos, with_labels = True, edge_color = edge_colors, node_color = "grey", node_size = 5, font_size = 8)  # ,  pos =, width = )

    if not os.path.exists("/".join(output_file.split("/")[:-1])):
        os.makedirs("/".join(output_file.split("/")[:-1]))

    # to save in a file :  
    plt.savefig(output_file)
    plt.close()

    # remove graph 

    # Execution time 
    print(f"Ends at {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")
    text_info = f"\nExecution time : {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes"

    with open(follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text_info)
    
    # execution_time (sec), execution_time (min), memory_usage (MiB), input_file_size (MiB)
    return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(os.path.getsize(input_file) / 1024)]


if __name__ == "__main__" : 

    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input_file", help = "Path to the file containing relations selected.")
    parser.add_argument("-o", "--output_file", help = "Path to the graph's file.")
    parser.add_argument("--follow_process_path", help = "Path to the file to save process data.")
    parser.add_argument("--min_digit", help = "The minimum digit authorized in patterns.")
    parser.add_argument("--max_digit", help = "The maximum digit authorized in patterns.")
    parser.add_argument("--max_distance", help = "Maximum distance between two pattern considered as similar.")
    parser.add_argument("-s", "--sep_table", help = "String that separates columns in tables.")

    args = parser.parse_args()

    get_TF_pattern_graph(args.max_digit, args.min_digit, args.max_distance, args.follow_process_path, args.input_file, args.output_file, args.sep_table)