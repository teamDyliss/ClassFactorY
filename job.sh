#!/bin/bash

#######################################################
###         Launch ClassFactorY queries             ###
###             author : Ève Barré                  ###
#######################################################

# Create ClassFactorY conda environment if necessary and activate it 
source activate ClassFactorY_env || source setup.sh; source activate ClassFactorY_env

# Launch ClassFactorY 
echo -e ""
echo -e "**********************************************************************************************************"
echo -e ""
echo -e "Welcome to ClassFactorY,"
echo -e "a Python tool for regulatory networks analysis, transcription factors and noncoding variants prioritization."
echo -e ""
echo -e "            __TF_"
echo -e "           |     |"
echo -e "       ____|  1  | "
echo -e "      |          |____"
echo -e "      |  2         3  |"
echo -e "      |_______________|"
echo -e ""
echo -e "author : Ève Barré, eve-barre@laposte.net"
echo -e "**********************************************************************************************************"
echo -e ""
     
python3 which_modules.py
python3 pipeline.py

# Deactivate environment 
conda deactivate