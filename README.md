
# ClassFactorY : a python tool supporting regulatory network analysis, transcription factor and noncoding variant prioritization.

## Table of contents

- [General descriptions](#general-descriptions)
- [How to install](#how-to-install)
- [How to use](#how-to-use)
- [General arguments](#major-arguments-of-modules)
- [Hierarchy of results directories](#result-directory-hierarchy)
- [Modules descriptions](#modules-descriptions)
  - [Part 1 : data preprocessing and selection preparation](#part-1-data-preprocessing-and-selection-preparation)
    - [relations_table_analysis_1](#relations_table_analysis)
    - [get_targets_regulators_1](#get_targets_regulators)
    - [get_annot_tf_1](#get_annot_tf)
    - [graph_go](#graph_go)
    - [get_MeSH](#get_MeSH)
  - [Part 2 : major transcription factors selection](#part-2-major-transcription-factors-selection)
    - [A_group_pattern](#group_pattern)
    - [B_select_tf](#select_tf)
    - [C_get_annot_tf](#get_annot_tf)
    - [D_select_variants](#select_variants)
  - [Part 3 : analysis of results and displays](#part-3-analysis-of-results-and-displays)
    - [relations_table_analysis_3](#relations_table_analysis)
    - [get_targets_regulators_3](#get_targets_regulators)
    - [compares_TF](#compares_TF)
    - [update_class](#update_class)
    - [TF_classification](#tf_classification)
    - [graphics modules](#graphics-modules)
  - [Notes and perspectives](#notes-and-perspectives)


## General descriptions


This tool helps the analysis of transcription factor (TF) - gene relations' regulatory network. 
ClassFactory offers many functions : 
- general statistical network analysis and graphical visualizations; 
- transcription factor annotations through several databases (pubmed, uniprot, gene ontology, MeSH terms...); 
- TF prioritization with a score in funciton of their annotations; 
- TF prioritization on their specificities and coverage for a expression gene pattern given and 
- non coding variants on transcription factor binding site identification from a vcf file given. 

See details on modules [here](#modules-descriptions).

See the developmental contexte in the following schema, ClassFactorY modules are in purpul.  


![ClassFactorY involvment](Tool_descriptions/ClassFactorY_role.png)


***


<!-- Relations' data must be a table having following columns : Transcription_Factor, Pattern_TF, Gene, Pattern_Gene, Reg (whatever the order). -->


## How to install

- Download ClassFactorY : 
  - from the [git](https://gitlab.com/EveBarre/ClassFactorY) by clicking on **Download**
  - **or** in command line in a terminal 

```
git clone https://gitlab.com/EveBarre/ClassFactorY.git
```

- Install conda [conda-4.10.1](https://conda.io/projects/conda/en/latest/user-guide/install/index.html#installation) and anaconda [anaconda-client-1.7.2](https://docs.anaconda.com/anaconda/install/)


## How to use

1) Be on a cluster where conda is available.

- If use Genouest, connect through your account with the display argument :

-->  For the first use run the following command and disconnect 

```
ssh -XC <your-login>@genossh.genouest.org
ssh-keygen -f ~/.ssh/id_slurm -t rsa -b 4096
cat ~/.ssh/id_slurm.pub >> ~/.ssh/authorized_keys
```

--> Then, simply run : 

```
ssh -X <your-login>@genossh.genouest.org
srun --x11 --pty bash
. /local/env/envconda3.sh
```

2) Launch ClassFactorY from its directory

Caution : For the first use, the environment creation can take a while.

```
cd <ClassFactorY_directory>
source job.sh
```

Both graphical user interface wil be generated, first for module to use selection then for theirs arguments. 

Click on "Start" bottum to launch the query and on "Close" when it is done.

Look on the working directory specified for results. 

/?\ To update ClassFactorY, run the following command 

```
git pull
```


## Modules descriptions

There are several modules with diverses functions, described below. Their usage is flexible and they are grouped in tree parts:

![Pipeline diagram](Tool_descriptions/pipeline_diagram.png)

*Pipeline_diagram*

***

### Part 1 Data preprocessing and selection preparation

[See part 1 modules details](#part_1-data-preprocessing-and-selection-preparation)

| Module's part_1 |                  Explanations                                     |
| --- | ------------------------  |
| [relations_table_analysis_1](#relations_table_analysis) | Relations_table_analysis_1 provides basic statistics about input data. For example it indicates how many genes and transcription factors (TF) compose the dataset, the ditribution of genes count per expression pattern, etc... Theses informations will help to better choose other modules' parameters (see group_pattern in part 2). |
| [get_targets_regulators_1](#get_targets_regulators) | Get_targets_regulators_1 is independant to others and describes each gene interactions with other genes in the network. The output is a table of each gene regulators (transcription factors that target it positively or negatively) and targets (if the gene is a transcription factor). If you are particularly interested in a candidate gene, you can have a first glance of its relations here. |
| [get_annot_tf_1](#get_annot_tf) | Get_annot_tf_1 provides several TF annotations in order to highlight known TF in the biological context in question. Possible annotations are :<br>a) if the TF is associated with Gene Ontology (GO) terms of interest (graph_go helps the choice);<br>b) the count of TF's quotations in pubmed database papers in tree possible conditions :<br>- The first count depict all TF citation whatever the paper,<br>- the second count one defines that articles must be annotated by a given general combinaison of keywords (Medical Subject Headings MeSH terms),<br>- and a more stringent stringent one using logical operators AND and OR too (see get_MeSH to choice);<br>c) keywords associated to TF in uniprot database, in order to visualize if TF is not directly annotated by GO terms of interest, in which biological process, disease, molecular function for example (ten possible user_adjustable keywords categories) it is known.<br>The serach of annotation can be done in the first part, before any TF selection or after (get_annot_tf_2). It is recommanded to use in the first part in case of multiple query because the annotated TF table can be directly reused instead of research again annotation for same TF (can be quite long). |
| [graph_go](#grapĥ_go) | To help the user to choose specific Gene Ontology (GO) terms to choose in get_annot_tf, it presents the hierarchical graph of a given GO term (for example, one may want to be more or less specific by choosing an descendant or a parent of the starting GO term).|
| [get_MeSH](#get_MeSH)  | This module guides MeSH terms choice for get_annot_tf queries.<br>From a set of articles (allows the user to use the litterature about the subject of interest) or of TFs, the module produces a table of MeSH terms found in this set and the count of articles per term. If a MeSH term is more represented, it can be one that well represents the biological context. |


***


### Part 2 Major transcription factors selection

[See part 2 modules details](#part_2-major-transcription-factors-selection)

The TF's selection is done on its coverage and specificity for a given gene pattern. Coverage depicts the part of the gene pattern relations regulated by the TF. Specificity represents the part of TF’s relations belonging to the gene pattern i.e., if a TF preferentially regulates one gene expression pattern over the others.


| Module's part_2 |                  Explanations                                     |
| --- | ------------------------  |
| [A_group_pattern](#group_pattern) | Both coverage and specificity are calculated as percentages and computed with the genes count per gene pattern. Some patterns regroup only few genes and may bias the coverage and specificity distributions. Therefore, this module proposes to delete gene patterns having small counts and/or gather them with their opposed patterns, on the hypothesis that opposed patterns are regulated oppositely. | 
| [B_select_tf](#select_tf) | Select_tf produces specificity and coverage calculations. Once computed, percentages are used to compare TF and select major relations. A threshold can be set as mean + one standard deviation, a quantile and/or a specific percentage. | 
| [C_get_annot_tf](#get_annot_tf) | TF's annotations can also be provided once TF are selected. | 
| [D_select_variants](#select_variants) | A link to variants can be done here. Variants located in a selected transcription factor binding site for a gene target are selected. |


***


### Part 3 Analysis of results and displays

[See part 3 modules details](#part_3-analysis-of-results-and-displays)

| Module's part_3 |                  Explanations                                     |
| --- | ------------------------  |
|[ relations_table_analysis_3](#relations_table_analysis) | After data modifications, it can be useful to launch relations_table_analysis_3 again to see the impact of selection on data. |
| [get_targets_regulators_3](#get_targets_regulators) | In the same way, get_targets_regulators_3 can also be launched with the selected TF-gene relations. |
|[compares_TF](#compares_TF) | Because query's possibilities are numerous, compares_TF allows to compares TF provided by two different manners. It precises common and specific TF from the two tables supplied. This new information can support final parameters choice. |
| [update_class](#update_class) | In order to follow TF selection, update_class (launched between each filtering module) classifies TF in a table where each line is a filter. It enables to check through which filter a TF of interest is lost for instance. | 
| [TF_classification](#tf_classification) | Attributes a score to each TF from annotations of interest defined. | 

Several graphs can be provided to picture TF's selected interactions, but modules are still in development. 

### Major arguments of modules 


| major modules | major arguments |
| --- | --- |
| graph_go | → go terms to have graph | 
| get_MeSH | → list of pubmed identifiers (or URI to papers) | 
|  | → email | 
| get_annot_tf | → go terms of interest |
| | → MeSH combination | 
| | → which keyword categories selected | 
| | → TF list : Transcription_Factor minimum column | 
| select_tf | → threshold selection type (quantile…) | 
| | → tabulated file having "Transcription_Factor, Pattern_TF, Gene, Pattern_Gene" minimum columns| 
| | → count table of gene per genes patterns | 
| select_variants | → reference genome if databases are queried | 
| | → table having Transcription_Factor, Gene (+/- Chrom, start, end, Reg) as columns | 
| | → TFBS file | 
| | → vcf file  | 
|  TF_classification | → Annotations terms classified in three level to score TF |
| | → TF list | 


### result directory hierarchy


| directories | output files example | modules associated |
| --- | --- | --- |
| Results_analysis | Account.md | all used | 
| | MeSH_count_table.csv |  get_MeSH | 
| | output_selected_TF_variants.csv | select_variants | 
| | targets_regulators_table.csv | get_targets_regulators | 
| | class_table.csv | update_class | 
| | TF_comparison.md | compares_TF | 
| Relations_tables | all_relations.csv | input | 
| | selected_relations.csv | select_tf | 
| | grouped_relations.csv | group_pattern | 
| TF_tables | selected_TF_table.csv | select_tf | 
| | selected_annotated_TF_table.csv | get_annot_tf | 
| | Initial_TF.csv | input | 
| graphics | go_graph.png | graph_go | 
| | col_to_analyse_hist.png | ~ all | 
| | col_to_analyse_box.png | | 
| | patterns_graph.png | graphs modules | 
| gene_per_gene_pattern_count_tables | gene_per_gene_pattern_count_table.csv | input | 
| | grouped_gene_per_gene_pattern_count_table.csv | group_pattern | 
 

***

## Modules description

![setup_pipeline_diagram](Tool_descriptions/setup_pipeline_diagram.png)

### setup

This script is the pipeline's setup and must be launch first (before pipeline.py. It stores which module use in setup.txt file (in the directory from the query launch). Modules are choosen through a graphical user interface (GUI) generated by Gooey. All module explanations are available on this GUI just oppend after setup.py launch. 

<p float="left">
  <img src="Tool_descriptions/gooey_setup.png" width="600" />
  <img src="Tool_descriptions/gooey_setup_finished.png" width="600" /> 
</p>


### pipeline


This script is the pipeline helping the anlysis of transcription factor - gene relations' regulatory network. The script setup.py must be executed before in order to defines which pipeline's module use. In a second step, pipeline.py can be execute to launch all modules selected with suitable arguments. Theses arguments are choosen through a graphical user interface (GUI) generated by Gooey.


<p float="left">
  <img src="Tool_descriptions/gooey_pipeline.png" width="600" />
</p>


[Back to the TOC](#table-of-contents)

## Part_1 Data preprocessing and selection preparation 


### relations_table_analysis



#### General description

Relations_table_analysis_1 provides basic statistics about input data. 
For example it indicates how many genes and transcription factors (TF) compose the dataset, 
the ditribution of genes count per expression pattern, etc... 


![relations_table_analysis_diagram](Tool_descriptions/relations_table_analysis_diagram.png)



#### Arguments

| Arguments | Explanations | Examples |
| --- | --- | --- |
| Relations_table | Path to the relation (TF-gene) table, mus have following columns label ('Transcription_Factor', 'Transcription_Factor', 'Gene', 'Pattern_Gene', 'Pattern_TF', 'Reg'). | work_directory/Relations_tables/my_Relations_table.csv |
| genes_per_gene_pattern_count_table | Path to the file containing the count of genes per gene's pattern, with columns labels 'Pattern_Gene', 'Nb_genes_per_gene_pattern' (.csv). | work_directory/genes_per_gene_pattern_count_tables/genes_per_gene_pattern_count_table.csv |
| follow_process_path | Path to file storing following process data (.md) | /home/user/workdirectory/Account.md |
| sep_table | String that separates columns in tables | '\t' |



### graph_go


#### General description

This script provides a hierargical graph of a Gene Ontology term given (parents and children) in order to
help the choice of GO term in get_annot_tf (to know if TFs are annotated by).
GO terms identifiers must be given, a list is possible (separates by a coma), as : 0002317, 0019724.

Comment : This module require stable internet connection in order to query databases.


![graph_go_diagram](Tool_descriptions/graph_go_diagram.png)



#### Arguments

| Arguments | Explanations | Examples |
| --- | --- | --- |
| follow_process_path | Path to file storing following process data (.md) | /home/user/workdirectory/Account.md |
| go_terms_graph | Label and gene ontology identifer for the one the parent graph is wanted | 0002317, 0019724 |
| graph_go_file | Path to the graph's file. (.png) | work_directory/Results_analysis/graphs/go_graphs/my_go_graph.png |



### get_MeSH


#### General description

This module guides MeSH terms choice for get_annot_tf queries. 
From a set of articles (allows the user to use the litterature about the subject of interest, pubmed) or of TFs, 
the module produces a table of MeSH terms found in this set and the count of articles per term. If a MeSH term is more represented, 
it can be one that well represent the biological context.
This set can also be all papers annotating transcription factors selected. 

Comment : This module require stable internet connection in order to query databases.


![get_MeSH_diagram](Tool_descriptions/get_MeSH_diagram.png)



#### Arguments 

| Arguments | Explanations | Examples |
| --- | --- | --- |
| follow_process_path | Path to file storing following process data (.md) | /home/user/workdirectory/Account.md |
| pmid_interest_file_path | Path to the file containing the list of pmid of interest (ex : 27191703) in a no-label column, can be  the pmid or the iri that contain it (ex : https://pubmed.ncbi.nlm.nih.gov/27191703/, select uri)) | work_directory/Results_analysis/pmid_interest.csv |
| output_mesh_file | Path to the file containing the table of mesh count (.csv). | work_directory/Results_analysis/MeSH_terms/my_mesh_file.csv |
| TF_table_MeSH | Path to the file containing the table of transcription factors (.csv). | work_directory/TF_tables/interest_TF_list.csv |
| max_pmid_per_TF_nb | Maximum number of papers annotating a transcription factor (5 to 500). | 50 |
| is_uri | Bool, True if list of pmid given is a uri containing pmid list, as https://pubmed.ncbi.nlm.nih.gov/12524387/.| X |
| email | email address to informs who launch query to nbci (mandatory). | firstname.name@example.fr |
| sep_table | String that separates columns in tables | '\t' |


***

[Back to the TOC](#table-of-contents)


## Part_2 Major transcription factors selection 


### group_pattern


#### General description

Both coverage and specificity are calculated as percentages and computed with the genes count per gene pattern. 
Some patterns regroup only few genes and may bias the coverage and specificity distributions. 
Therefore, this module proposes to delete gene patterns having small counts and/or gather them 
with their opposed patterns, on the hypothesis that opposed patterns are regulated oppositely.


![group_pattern_diagram](Tool_descriptions/group_pattern_diagram.png)


#### Arguments

| Arguments | Explanations | Examples |
| --- | --- | --- |
| Relations_table | Path to the relation (TF-gene) table, mus have following columns label ('Transcription_Factor', 'Transcription_Factor', 'Gene', 'Pattern_Gene', 'Pattern_TF', 'Reg'). |  work_directory/Relations_tables/my_Relations_table.csv |
| genes_per_gene_pattern_count_table | Path to the file containing the count of genes per gene's pattern, with columns labels 'Pattern_Gene', 'Nb_genes_per_gene_pattern' (.csv). | work_directory/genes_per_gene_pattern_count_tables/genes_per_gene_pattern_count_table.csv |
| follow_process_path | Path to file storing following process data (.md) | /home/user/workdirectory/Account.md |
| sep_table | String that separates columns in tables | '\t' |
| output_relations_group_file | Path to the output file (table containing transcription factor gathered by gene's pattern groups). | work_directory/Relations_tables/grouped_pattern_relations.csv |
| output_count_table | Path to the new genes per genes' profiles count table. | work_directory/genes_per_gene_pattern_count_tables/grouped_pattern_genes_per_gene_pattern_count.csv |
| max_genes_per_pattern_number | The maximum number of gene per gene's pattern authorizted to allow group with the opposed pattern. | 150 |
| delete_small_gene_patterns | Deletes instead of group gene's patterns with its opposed. Give the gene count threshold. | 15 |



### select_tf


#### General description

The TF's selection is done on its coverage and specificity for a gene pattern. 
Coverage depict which part of the gene pattern is regulated by a specific TF. 
Specificity represents the number of targets of a TF which belong to a specific pattern i.e., 
if a TF preferentially regulates one gene expression pattern over the others. 
Select_tf produces specificity and coverage calculations. 
Once computed, percentages are used to compare TF and select major relations. 
A threshold can be set as mean + one standard deviation, a quantile and/or a specific percentage. 


![select_tf_diagram](Tool_descriptions/select_tf_diagram.png)


#### Arguments

| Arguments |Explanations | Examples |
| --- | --- | --- |
| Relations_table | Path to the relation (TF-gene) table, mus have following columns label ('Transcription_Factor', 'Transcription_Factor', 'Gene', 'Pattern_Gene', 'Pattern_TF', 'Reg'). | work_directory/Relations_tables/my_Relations_table.csv |
| genes_per_gene_pattern_count_table | Path to the file containing the count of genes per gene's pattern, with columns labels 'Pattern_Gene', 'Nb_genes_per_gene_pattern' (.csv). | work_directory/genes_per_gene_pattern_count_tables/genes_per_gene_pattern_count_table.csv |
| follow_process_path | Path to file storing following process data (.md) | /home/user/workdirectory/Account.md |
| sep_table | String that separates columns in tables | '\t' |
| store_relation_table_file | Path to the selected reltations' table file, if wanted to be returned (.csv). | work_directory/Relations_tables/selected_relations.csv | 
| stores_table_TF_file | Path to the selected transcription factors file, if wanted to be returned (.csv). | work_directory/TF_tables/selected_TF.csv |
| threshold_type | The type of threshold | mean+std (quantile : q50, q75...) | 
| percentage_threshold_cov_spe | Minimum percentage of TF's coverage and specificy for a gene_pattern in decimal to filter | '0.75,0.80' (or '0.75,No' meaning 'cov,spe') | 
| preselected_table | Path to  the preselected table but with data on the selection (.csv). | work_directory/Relations_tables/preselected_relations.csv | 
| columns to conserve | Specifies columns in the output table to concerve (example for all columns : 'Transcription_Factor', 'Pattern_TF', 'Nb_genes_per_TF', 'Nb_relations_selected', 'selected_pattern_nb', 'Patterns_targeted_pos', 'Patterns_targeted_neg', 'Patterns_targeted'). | X (bool for each column) |
<!-- | are_patterns_given | Bool, if patterns' columns for transcripton facotr and gene are given. | X |
| is_regulatory_direction_given | Bool, if regylatory direction between transcription factor and gene are given | X |
 -->

### get_annot_tf


Get_annot_tf provides several TF annotations in order to highlight known TF in the biological context in question. 
Possible annotations are : 
- a) if the TF is associated with Gene Ontology (GO) terms of interest (graph_go helps the choice);
- b) the count of TF's quotations in pubmed database papers in tree possible conditions : 
    - The first count depict all TF citation whatever the paper, 
    - the second count one defines that articles must be annotated by a given general combinaison of keywords (Medical Subject Headings MeSH terms), 
    - and a more stringent stringent one using logical operators AND and OR too (see get_MeSH to choice);
- c) keywords associated to TF in uniprot database (https://sparql.uniprot.org/sparql/), in order to visualize if TF is not directly annotated by GO terms of interest, 
in which biological process, disease, molecular function for example (ten possible user_adjustable keywords categories) it is known.
The serach of annotation can be done in the first part, before any TF selection or after (get_annot_tf_2). 

Comment : This module require stable internet connection in order to query databases.
Warning : It is possible that the database queried is momentaly not accessible, please try to request the day after. 


![get_annot_tf_diagram](Tool_descriptions/get_annot_tf_diagram.png)


#### Arguments

| Arguments | Explanations | Examples |
| --- | --- | --- |
| Relations_table | Path to the relation (TF-gene) table, mus have following columns label ('Transcription_Factor', 'Transcription_Factor', 'Gene', 'Pattern_Gene', 'Pattern_TF', 'Reg'). |  work_directory/Relations_tables/my_Relations_table.csv |
| genes_per_gene_pattern_count_table | Path to the file containing the count of genes per gene's pattern, with columns labels 'Pattern_Gene', 'Nb_genes_per_gene_pattern' (.csv). | work_directory/genes_per_gene_pattern_count_tables/genes_per_gene_pattern_count_table.csv |
| follow_process_path | Path to file storing following process data (.md) | /home/user/workdirectory/Account.md |
| sep_table | String that separates columns in tables | '\t' |
| email | email address to informs who launch query to nbci (mandatory). | firstname.name@example.fr |
| go_interest | String of go terms (label:id) of interest | transcription regulator activity:0140110, cellular developmental process:0048869, immune system process:0002376, lymphocyte activation:0046649, B cell activation:0042113, plasma cell differenciation:0002317, B cell differentiation:0030183 |
| unwanted_keywords | Specific keywords that are not wanted  | 'DNA-binding, Repressor, Activator, Transcription regulation' | 
| strict_MeSH_combinaition| Maximum list of MeSH terms (labels) that papers must be annotated with to be counted, (choice help with get_MeSH module) | (Plasma Cells OR B-Lymphocytes OR Lymphocyte Activation OR Germinal Center) AND (Transcription Factors OR Gene Expression Regulation) AND Cell Differentiation | 
| large_MeSH_combinaition | Maximum list of MeSH terms (labels) that papers must be annotated with to be counted, (choice help with get_MeSH module) | (B-Lymphocytes OR Plasma Cells) | 
| get_G_quotations_nb | Bool, if general papers' quotation for this TF number must be return as a column. | X |
| already_annotation_table | Path to the all transcription factor annotated table. | work_directory/TF_tables/my_all_TF_annotated_table.csv |
| dict_annotations_columns_id | Dictionnary of annotations columns number given by an other query (in the follow process file) to reuse annotations of a table. | {'column_G_quotations': 2, 'column_strict_MeSH_quotations': 5, 'column_large_MeSH_quotations': 3, 'columns_keywords_categories': [14], 'columns_go_terms': [7, 8, 9, 10, 11, 12, 13]} |
| all_TF_table_annotated or <br>TF_table_annotated_file | Stores file's name's transcription factors annotated. | work_directory/TF_tables/my_TF_list_annotated.csv |
| categories_keywords | Keywords category to allow to be display as a column (Bioligocal process, molecular dunction, cellular component, coding sequence diversity, developmental stage, disease, domain, ligand, post_translational modification, technical term)| X (bool for each categories) |




### select_variants


Select_variants class variants if they are located on a transcription factor given binding sites (TFBS) on target gene's regulatory regions.
If data are not given, TFBS can be obtained on JASPAR REST API and gene coordinates in Ensembl REST API. 

VCF file must have #CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO first line. 

![select_variants_diagram](Tool_descriptions/select_variants_diagram.png)


#### Arguments

| Arguments | Explanations | Examples |
| --- | --- | --- |
| reference_genome | Reference genome used and to use to search genome coordinates. | hg19 or hg38 or grch38 or grch37 |
| JASPAR_version | If transcription factor binding sites must be search, precise which version of JASPAR database use. | 2020 | 
| TF_gene_region_file | Relations_table path, can contain or not regions coordinates. | workdirectory/Relations_table/TF_gene_region_file.csv |
| output_variants_file | Path to the file to output (with variants per transcription factor found). | workdirectoey/Results_analysis/TF_variants.csv |
| sep_table | String that separates columns in transcription table of interest. | "\t" |
| VCF_file | Path to the Variant Calling Format file. | workdirectory/variants.vcf |
| regulatory_gene_distance | Distance between a gene and its regulatory regions to consider in bases number. | 500000  (< 2000000) |
| are_regulatory_region_given | Bool, if regions' genomic coordinates are already given in the relation table (start, end, chrom). | X |
| TFBS_file | Path to the transcription binding sites file if already search, otherwise, search on JASPAR. | workdirectory/TFBS.bed |
| follow_process_path | Path to the file to save process data. | workdirectory/Results_analysis/Account.md |
| is_regulatory_direction_given | Bool, if regulatory direction is given as a column in relations table. | X |



***


[Back to the TOC](#table-of-contents)


## Part_3 Analysis of results and displays


#### /!\ graphs modules are still in development 

### get_TF_pattern_graph 


This script provides the graph figure (.png) of TF' patterns selected.
Relation are about if tf's patterns are similar or quite opposed (+/- 1 authorized for one digit of the pattern).


### get_gene_pattern_per_TF_graph


Script get_pattern_graph_TF stores the graph figure (.png) of pattern's targeted by a given transcription factor.
Relation are about if gene's patterns are similar or quite opposed (+/- 1 authorized for one digit of the pattern).

- direct opposed edge = red
- undirect opposed edge = orange
- similar edge = green 
- positive regulation = "+"
- negative regulation = "-"

A command line to launch can be like : 
python get_pattern_graph_TF -i TF_tables\TF_table_q75.csv -tf PRDM1 

### get_gene_pattern_per_TF_pattern_graph


This script provides the graph figure (.png) of genes' pattern's targeted by a transcription factor's pattern given.
Relation are about if gene's patterns are similar or quite opposed (+/- 1 authorized for one digit of the pattern).
 (see get_gene_pattern_per_TF_graph)

### TF_classification


#### General description


This script attributes a score to each TF from a list given from their annotations (on GO terms, Uniprot keywords, quotations number and percentages).
For each annotaiton type, three percision level defining the biological context (each having three points level). 
It has been applied a coefficient of 4 for points given with GO terms and Uniprot keywords because quotations percentage have 4 assess axes 
(in order to provides same weigth to each annotations categories in the score).
This score definition can be tougth again to be more flexible.


#### Arguments


| Arguments | Explanations | Examples |
| --- | --- | --- |
| TF_annotated_table | Path to the file containing TF' informations. | \work_diretory\TF_tables\TF_annotated_table.csv |
| keywords_dic | Dictionnary of keywords's categories as keys and list of keywords of interest more (first) and less (last) precise in a list | {"Biological process" : [["Adaptive immunity", "Differentiation", "Adaptive immunity", "Innate immunity", "Antiviral defense", "Inflammatory response", "Host-virus interaction", "Immunity", "unfolded protein response"], ["Cell cycle", "Apoptosis", "Stress response"]]} | 
| GO_interest_list | list of GO terms (and their count of TF in GO) of interest list (first list = well known, second list : known, last list : described | [["3_B cell mediated immunity", "30_B cell activation", "8_B cell differentiation", "0_plasma cell differentiation"], ["69_lymphocyte activation", "11_adaptive immune response"], ["645_cellular developmental process", "161_immune system process"]] | 
| threshold_large_quot_nb_1 | First threshold of large quotations number : TF known | 16 | 
| threshold_large_quot_nb_2 | Second threshold of large quotations number : TF well known | 63 | 
| threshold_strict_quot_nb_1 | First threshold of strict quotations number : TF known | 3 | 
| threshold_strict_quot_nb_2 | Second threshold of strict quotations number : TF well known | 14 | 
| threshold_large_quot_perc_1 | First threshold of large quotations percentage : TF known | 3 | 
| threshold_large_quot_perc_2 | Second threshold of large quotations percentage : TF well known | 10 | 
| threshold_strict_quot_perc_1 | First threshold of strict quotations percentage : TF known | 3 | 
| threshold_strict_quot_perc_2 | {threshold_strict_quot_perc_2} |
| follow_process_path | "Second threshold of strict quotations percentage : TF well known | 10 | 
| sep_table | String that separates columns in tables| \t |
| TF_scored | Path to the TF scored table | \work_diretory\TF_tables\TF_scored_table.csv | 

![update_class_diagram](Tool_descriptions/TF_classification_diagram.png)


### compares_TF


This scripts compares transcription factors' lists and returns TF that are specific and common from tables.
Theres two tables must have a "Transcription Factor" column label. 

#### Arguments

| Arguments | Explanations | Examples |
| --- | --- | --- |
| TF_table_1 | Path to the first file containing Transcription factors to compare (csv). | workdirectory/TF_tables/my_TF_table_1.csv |
| TF_table_2 | Path to the second file containing Transcription factors to compare (csv). | workdirectory/TF_tables/my_TF_table_2.csv |
| follow_process_path | Path to file storing following process data (.md) | /home/user/workdirectory/Account.md |
| sep_table | String that separates columns in tables | '\t' |
| comparison_name | Name to given to the comparison | threshold q50 vs q75 | 
| output_file_compares_TF | Path to output file containing comparison (markdown or txt). | workdirectory/Results_Analysis/TF_comparison.md |


![compares_TF_diagram](Tool_descriptions/compares_TF_diagram.png)


### update_class


#### General description

In order to follow TF selection, update_class (launched between each filtering module) classifies TF in a table 
where each line is a filter. It enables to check through which filter a TF of interest is lost for instance.
Filters could be : 

  - all TF
  - differential expression +/- constant
  - compatibility table 
  - specificity + coverage
  - ? : annotations, publication's quotation number ... 

#### Arguments


| Arguments | Explanations | Examples |
| --- | --- | --- |
| Relations_table | Path to the relation (TF-gene) table, mus have following columns label ('Transcription_Factor', 'Transcription_Factor', 'Gene', 'Pattern_Gene', 'Pattern_TF', 'Reg'). | work_directory/Relations_tables/my_Relations_table.csv |
| genes_per_gene_pattern_count_table | Path to the file containing the count of genes per gene's pattern, with columns labels 'Pattern_Gene', 'Nb_genes_per_gene_pattern' (.csv). | work_directory/genes_per_gene_pattern_count_tables/genes_per_gene_pattern_count_table.csv |
| follow_process_path | Path to file storing following process data (.md) | /home/user/workdirectory/Account.md |
| sep_table | String that separates columns in tables | '\t' |
| class_table | Path to table that class all TF in function of filters used. | workdirectory/Results_Analysis/class_table.md
| go_terms_as_column | Go terms to keep as column in the classification (it gives the proportion of annotated selected TF).| transcription regulator activity, cellular developmental process, immune system process, lymphocyte activation, B cell activation, plasma cell differenciation, B cell differentiation | 
| quotations_count_as_column | Papers quotation count ('G_quotations_nb', 'strict_quotations_nb', 'large_quotations_nb' to keep as column in the classification (gives the annoated TF count).| X (bool for each) |
| all_TF_annotated_table_path | Path to the annotated transcription factors table. | workdirectory/TF_tables/my_all_annotated_TF.csv |
| filter_name | Filter name applied on transcription factors. (if nothing in part_2) | threshold q75 |
| TF_just_filtered | List of transcription factors just filtered. (if nothing in part_2) | workdirectory/TF_tables/TF_to_filter.csv |
| all_initial_TF | List of transcription factors known initially. (if get_annot_tf_1) | workdirectory/TF_tables/all_initial_TF.csv |


![update_class_diagram](Tool_descriptions/graph_go_diagram.png)


[Back to the TOC](#table-of-contents)


## Notes and perspectives

[Link to a google document](https://docs.google.com/document/d/1g5gaIOZd1FyYY3wuBXMzv-Epdnu5T45kQOACMqhHy9I/edit?usp=sharing)

<!-- ## Part 1 _ Data preprocessing and query preparation
 -->

<!-- | Module's name | Description | Input | Prerequisite |
| --- | --- | --- | --- |
| relations_table_analysis_1 | general statistics on data | relation_table_path, genes_per_gene_pattern_count_table_path ||
| graph_go | hierarchical graph of go terms | go_terms, output_file ||
| get_MeSH | papers' MeSH count | pmid table and if is uri or TF table and max pmid per TF | Bio (Entrez, Medline) |
| get_annot_tf_1 | TF annotations (go, quotations number, keywords) | TF list, path_TF_table_annotated, list_go_interest, list_unwanted_leywords, list_categories_keywords, max_MeSH_terms, min_MeSH_terms, get_G_quotations_nb | Bio (Entrez), SPARQLWrapper |
| get_targets_regulators_1| Genes targets and regulators | relations_table, Genes, result_path || -->

<!-- | get_rel_tf_1 | selects specific genes targets and regulators | relations_table, Gene, result_path || ####

***

## Part 2 _ Selection of major transcription factors



| Module's name | Description | Input | Prerequisite |
| --- | --- | --- | --- |
| A_group_pattern | gathers (or deletes) opposed gene's patterns | min and max digit (in patterns), genes_per_gene_pattern_count_table, relations_table, max_genes_per_pattern_nb, gene_patterns_to_delete, delete_small_gene_patterns ||
| B_select_tf | select TF on their coverage and specificity | table_tf, threshold_type, percentage_threshold_cov_spe, columns_TF, genes_patterns_to_delete, preselected_table, table_count_gene_by_pattern ||
| C_get_annot_tf | TF annotations (go, quotations number, keywords) | TF list, path_TF_table_annotated, list_go_interest, list_unwanted_keywords, list_categories_keywords, max_MeSH_terms, min_MeSH_terms, get_G_quotations_nb | Bio (Entrez), SPARQLWrapper |

***

## Part 3 _ Analyse of resutls and display



| Module's name | Description | Input | Prerequisite |
| --- | --- | --- | --- |
| get_targets_regulators_3 | Genes targets and regulators | relations_table, Genes, result_path ||
| get_TF_pattern_graph | TF patterns' interaction's graph (is similar or opposed) | max and min digit (in patterns), max distance, relation_table | networkx |
| get_gene_pattern_per_TF_graph | gene's patterns interaction per TF's graph | max and min digit (in patterns), max distance, relation_table, TF | networkx |
| get_gene_pattern_per_TF_pattern_graph | gene's patterns interaction per TF's pattern's graph | max and min digit (in patterns), max distance, relation_table, TF patterns | networkx |
| compares_TF | compares two TF tables  | TF_table_1, TF_table_2, comparison_name, output_file ||
| update_class | TF classification depends on filters | filter_name, TF_table, dict_annot_columns_id ||
| relations_table_analysis_3 | general statistics on data | relation_table_path, genes_per_gene_pattern_count_table_path ||

<!-- | get_rel_tf_3 | Specific genes targets and regulators | relations_table, Gene, result_path || ####


