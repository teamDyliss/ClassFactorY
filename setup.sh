#!/bin/bash

#######################################################
### Creates virtual conda env and imports packages ###
#######################################################

echo -e "Creating virtual conda environment for ClassFactorY, please wait, this can take a while...."

conda create --name ClassFactorY_env

conda activate ClassFactorY_env; conda install python==3.8.5; conda install -c conda-forge networkx==2.5.1; conda install numpy==1.20.2; conda install -c conda-forge matplotlib==3.4.1; conda install pandas==1.2.4; conda install -c conda-forge pandas-profiling==2.11.0; conda install -c conda-forge gooey==1.0.8; conda install -c conda-forge SPARQLWrapper==1.8.5; conda install -c anaconda biopython; conda install tabulate==0.8.9; conda install -c bioconda bedtools==2.30.0  # , conda install -c bioconda pybedtools==0.8.2 ; conda install -c conda-forge libcanberra; conda install -c conda-forge gtk3

# incompatible, conflict :
# conda install -c bioconda pybedtools

conda deactivate

