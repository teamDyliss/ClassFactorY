#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author : Ève Barré

--> General description : 

Both coverage and specificity are calculated as percentages and computed with the genes count per gene pattern. 
Some patterns regroup only few genes and may bias the coverage and specificity distributions. 
Therefore, this module proposes to delete gene patterns having small counts and/or gather them 
with their opposed patterns, on the hypothesis that opposed patterns are regulated oppositely.

--> Arguments : 

    "-i", "--input_file" : Path to the file containing relations' information.
    "-gct", "--genes_per_gene_pattern_count_table" : Path to the file containing the count of genes per gene's pattern.
    "-or", "--output_relations_file" : Path to the output file (table containing transcription factor gathered by gene's pattern groups).
    "-oc", "--output_count_table" : Path to the new genes per genes' profiles count table.
    "-max", "--max_genes_per_pattern_number" : The maximum number of gene per gene's pattern authorizted to allow group with the opposed pattern.
    "-del", "--delete_gene_patterns" : Deletes instead of group gene's patterns with its opposed. Give the gene count threshold.

--> In pipeline : 

This group_pattern module allows to reduce calculations biais on specificities and coverage at TF's selection. 
That is why, this module must be launch before select_tf module who calculates both parameters (coverage and specificity).
Several parameters are user-adjustable : gene's count threshold from the one gene's pattern are deleted from analysis or allowed to gatehr with its opposed one.
Theres thresholds can be chosen through data satistics given by relations_table_analysis_1 module. 
The pattern gather impact can be assess again by relaunch relations_table_analysis_3 after this group_pattern module.
The effect on gene's patterns targeted : 
    - reduces specificities (raise relations in a pattern given by adding those of its opposed one) : 
    - reduces coverage (raise pattern genes's count by adding those of opposed one)


--> Usage tips : 

Genes count around 5 are effectively quite low and could affect coverage and specificities that could be excessively  high. 
Through our tests we recommand threshold use of ... to deletes patterns and ... to gather.


--> Proceeding : 

    - writes in a file data on the process (--follow_process_path) : 
        - module's name 
        - date and time of query
        - arguments used
        - execution time and memory usage

    - imports relations table (TF-gene) (drop duplicates) (-i) and gene per gene pattern count table (-gct)
    - deletes gene's pattern to delete (--delete_gene_patterns))
    - selects genes' pattern to gather with its opposed on (--max_genes_per_pattern_nb)
    - defines genes' pattern allowed to group with its opposed by a gene number threshold (-max)
    - defines if the opposed patterns is touched by a same TF oppositely: 
        - defines all gene patterns and its regulatory direction involved with each TF 
        - adds a new columns notes the gene pattern opposed pattern if is touched by the TF involved in this tf-gene relation 
    - for all tf-gene relations selected to be able to group with their opposed : 
        - change the gene's pattern label by its opposed
        - change regulatory direction by its opposed
        - adds theirs new relations to the table
    - deletes gene's patterns with small gene count after gathering
    - defines new genes per gene pattern count by adding new relations number per gene pattern 
    - stores new gene per gene pattern count table (oc)
    - stores new relations table (or)

sum up : 
1. Deletes patterns to delete
2. Gathers patterns to group
3. Deletes patterns with small count (threshold)

--> Execution time and memory evolution : 

'''

# Adds info on process (times ...)

import pandas as pd
import argparse 
import time 
import datetime 
import os
import matplotlib.pyplot as plt
from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage
import pipeline


# functions needed : 
def get_new_count(row):
    '''
    '''
    new_count = row["Nb_genes_per_gene_pattern"]
    if str(row["nw_genes_nb"]) != "nan":
        return new_count + row["nw_genes_nb"]
    return new_count

def get_opp_reg(Reg) -> str : 
    '''
    '''
    if Reg == "-" : 
        return "+"
    return "-"
    
def find_opposed_pattern(row, max_digit, min_digit):
    '''
    Returns opposed pattern if related to the current transcription factor with on opposed directon. 

    Args : 

    Returns : 

    '''

    Pattern_Gene = row["Pattern_Gene"]
    opposed = pipeline.get_opposed(Pattern_Gene, max_digit, min_digit)
    list_patterns = row["genes_patterns"].split(",")    
    list_patterns_reg = row["genes_patterns_reg"].split(",")

    if (opposed in list_patterns) :
        index_opposed = list_patterns.index(opposed)
        if (list_patterns_reg[index_opposed] != row["Reg"]) :  # just if their regulatory direction are opposed too
            return {"Transcription_Factor":row["Transcription_Factor"], "Pattern_Gene":Pattern_Gene, "opposed_pattern" : opposed}
    return {"Transcription_Factor":row["Transcription_Factor"], "Pattern_Gene":Pattern_Gene, "opposed_pattern" : "No"}


def group_pattern(max_digit, min_digit, follow_process_path, input_file, genes_per_gene_pattern_count_table, output_count_table, output_relations_file, max_genes_per_pattern_number, delete_small_gene_patterns, gene_patterns_to_delete, sep_table, TF_patterns_to_delete) :
    '''
    '''
    t0 = time.time()

    max_digit = int(max_digit)
    min_digit = int(min_digit)

    # asserts is a .md file 
    if follow_process_path[-3:] != ".md" :
        raise ValueError(f"'follow_process_path' must be a markdown (.md) file not {follow_process_path.split('.')[-1]}.")

    if max_digit <= min_digit : 
        raise ValueError(f"Minimum pattern's digit {min_digit} must be smaller than the maximum {max_digit}.")
    
    if ((max_digit > 9) | (min_digit > 9)) :
        raise ValueError(f"Patterns' digit must be digit (0 to 9 only).")
    

    # markdown file to follow process :
    text_info = (f"""

# <font color=orange>Module group_pattern.py :</font> 

lauched at {datetime.datetime.today()}

with arguments : 

| Arguments | |
| --- | --- |
| max_digit | {max_digit} |
| min_digit | {min_digit} |
| genes_per_gene_pattern_count_table | {genes_per_gene_pattern_count_table} |
| follow_process_path | {follow_process_path} |
| input_file | {input_file} |
| output_count_table | {output_count_table} |
| sep_table | {sep_table} |
| output_relations_file | {output_relations_file} |
| max_genes_per_pattern_number | {max_genes_per_pattern_number} |
| delete_small_gene_patterns | {delete_small_gene_patterns} |
| gene_patterns_to_delete | {gene_patterns_to_delete} |
| TF_patterns_to_delete | {TF_patterns_to_delete} |

""")
    print(f"""

********************************************************************************
--> Module group_pattern.py :
lauched at {datetime.datetime.today()}
********************************************************************************

""")

    # Checks existence of input files : 
    if not os.path.exists(input_file):
        raise ValueError(f"{input_file} does not exist. Change input.")
    
    if not os.path.exists(genes_per_gene_pattern_count_table):
        raise ValueError(f"{genes_per_gene_pattern_count_table} does not exist. Change input.")

    if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
        os.makedirs("/".join(follow_process_path.split("/")[:-1]))

    with open(follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text_info)
    
    # Checks if arguments are missing : 
    if not input_file : 
        raise ValueError("Input file missing. (--input_file)")

    input_size = os.path.getsize(input_file) + os.path.getsize(genes_per_gene_pattern_count_table)

    # Input data : 
    Relation_table = pd.read_csv(input_file, sep = sep_table, header = 0, usecols = ["Gene", "Pattern_Gene", "Transcription_Factor", "Pattern_TF", "Reg"]).drop_duplicates()
    Relation_table["Pattern_Gene"] = Relation_table["Pattern_Gene"].astype(float).astype("Int64").astype(str)
    Relation_table["Pattern_TF"] = Relation_table["Pattern_TF"].astype(float).astype("Int64").astype(str)

    genes_per_gene_pattern_count_table = pd.read_csv(genes_per_gene_pattern_count_table, sep = sep_table, header = 0, usecols = ["Pattern_Gene", "Nb_genes_per_gene_pattern"])
    genes_per_gene_pattern_count_table["Pattern_Gene"] = genes_per_gene_pattern_count_table["Pattern_Gene"].astype(str)

    # Deletes duplicated TF_Gene relations:
    # shape1 = Relation_table.shape[0]
    # Relation_table = Relation_table.drop_duplicates()
    # shape2 = Relation_table.shape[0]
    
    # deletes gene's pattern unwanted : 
    if gene_patterns_to_delete :
        for gene_pattern in gene_patterns_to_delete.split(', ') :
            Relation_table = Relation_table[Relation_table["Pattern_Gene"] != gene_pattern]
            genes_per_gene_pattern_count_table = genes_per_gene_pattern_count_table[genes_per_gene_pattern_count_table["Pattern_Gene"] != gene_pattern]

    # deletes TF's pattern unwanted : 
    if TF_patterns_to_delete :
        for TF_pattern in TF_patterns_to_delete.split(', ') :
            Relation_table = Relation_table[Relation_table["Pattern_TF"] != TF_pattern]

    # Filters pattern that must be able to be grouped with its oppposed pattern or patterns to delete: 
    if max_genes_per_pattern_number : 
        Patterns_to_group = genes_per_gene_pattern_count_table[genes_per_gene_pattern_count_table["Nb_genes_per_gene_pattern"] < int(max_genes_per_pattern_number)]
        Patterns_per_TF_table = Relation_table[["Transcription_Factor", "Pattern_Gene"]].drop_duplicates().merge(Patterns_to_group[["Pattern_Gene"]], on = ["Pattern_Gene"], how = "inner")
        Patterns_to_group = []
        Patterns_per_TF_table = Patterns_per_TF_table[["Transcription_Factor", "Pattern_Gene"]]  # Not to have column "Nb_genes_per_gene_pattern" any more
        
        if Patterns_per_TF_table.shape[0] != 0 :
        
            # 1) defines if TF touch 2 opposed patterns with opposed regulation :

            # defines all patterns involving a given transcription factor and its reg
            Patterns_per_TF_table = Patterns_per_TF_table.groupby(["Transcription_Factor"])["Pattern_Gene"].apply(",".join).reset_index().rename(columns = {"Pattern_Gene":"genes_patterns"})
            Reg_patterns_per_TF_table = Relation_table[["Transcription_Factor", "Pattern_Gene", "Reg"]].drop_duplicates().groupby(["Transcription_Factor"])["Reg"].apply(",".join).reset_index().rename(columns = {"Reg":"genes_patterns_reg"})

            # Adds columns of all genes pattern and reg per TF
            Is_opposed_column = Relation_table[["Transcription_Factor", "Pattern_Gene", "Reg"]].drop_duplicates().merge(Patterns_per_TF_table, on = ["Transcription_Factor"])
            Is_opposed_column = Is_opposed_column.merge(Reg_patterns_per_TF_table, on = ["Transcription_Factor"])
            Reg_patterns_per_TF_table = []
            Patterns_per_TF_table = []

            if Is_opposed_column.shape[0] != 0 : 

                # 2) Adds new relations : 

                print(f"Calculates new relations to add, {datetime.datetime.today()}.")
                t0 = time.time()
                Is_opposed_column = Is_opposed_column.apply(lambda row: find_opposed_pattern(row, max_digit, min_digit), axis = 1, result_type = "expand")
                Relation_table = Relation_table.merge(Is_opposed_column, on = ["Transcription_Factor", "Pattern_Gene"], how = "left")
                Is_opposed_column = []

                t1 = time.time()
                t_fin = round((t1 - t0), 3)
                print(f"Done in {t_fin} seconds.\n") 

                # Replaces names of gene's pattern by its opposed pattern in order to have the relation in both pattern opposed
                # and changes regulation direction
                New_rel_to_add = Relation_table[Relation_table["opposed_pattern"] != "No"]

                New_rel_to_add["Reg"] = New_rel_to_add.apply(lambda row : get_opp_reg(row["Reg"]), axis = 1)

                New_rel_to_add = New_rel_to_add.drop(columns = ["Pattern_Gene"])
                New_rel_to_add = New_rel_to_add.rename(columns = {"opposed_pattern":"Pattern_Gene"})
                Relation_table = pd.concat([Relation_table, New_rel_to_add], axis = 0)
                # Not urge to sort ?


                # Defines new genes count :
                New_gene_per_pattern = New_rel_to_add[["Pattern_Gene", "Gene"]].groupby(["Pattern_Gene"])["Gene"].nunique().rename("nw_genes_nb").reset_index()

                New_gene_per_pattern["Pattern_Gene"] = New_gene_per_pattern["Pattern_Gene"].astype(str)

                genes_per_gene_pattern_count_table = genes_per_gene_pattern_count_table.merge(New_gene_per_pattern, on = ["Pattern_Gene"], how = "outer")

                genes_per_gene_pattern_count_table["new_count"] = genes_per_gene_pattern_count_table.apply(lambda row : get_new_count(row), axis = 1)


                #genes_per_gene_pattern_count_table["new_count"] = genes_per_gene_pattern_count_table["Nb_genes_per_gene_pattern"] + genes_per_gene_pattern_count_table["nw_genes_nb"]
                genes_per_gene_pattern_count_table = genes_per_gene_pattern_count_table[["new_count", "Pattern_Gene"]]
                genes_per_gene_pattern_count_table = genes_per_gene_pattern_count_table.rename(columns = {"new_count":"Nb_genes_per_gene_pattern"})

    # deletes relations with gene's patterns having genes count inferious than threshold given :
    if delete_small_gene_patterns :
        genes_per_gene_pattern_count_table = genes_per_gene_pattern_count_table[genes_per_gene_pattern_count_table["Nb_genes_per_gene_pattern"] > int(delete_small_gene_patterns)]
        Relation_table = Relation_table.merge(genes_per_gene_pattern_count_table[["Pattern_Gene"]], on = ["Pattern_Gene"], how = "right")

    # Stores new gene per genes pattern counts' table
    if not os.path.exists("/".join(output_count_table.split("/")[:-1])):
        os.makedirs("/".join(output_count_table.split("/")[:-1]))

    genes_per_gene_pattern_count_table[["Pattern_Gene", "Nb_genes_per_gene_pattern"]].to_csv(output_count_table, sep = sep_table, index = None)

    if not os.path.exists("/".join(output_relations_file.split("/")[:-1])):
        os.makedirs("/".join(output_relations_file.split("/")[:-1]))

    # Stores new relations' table
    Relation_table[["Transcription_Factor", "Pattern_TF", "Pattern_Gene", "Gene", "Reg"]].to_csv(output_relations_file, sep = sep_table, index = None)


    # Execution time 
    print(f"Ends at {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")
    text_info = f"\nExecution time : {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes"

    with open(follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text_info)
    
    # execution_time (sec), execution_time (min), memory_usage (MiB), input_file_size (MiB)
    return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(input_size / 1024)]

if __name__ == "__main__" : 

    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input_file", help = "Path to the file containing relations' information, must have columns' labels : 'Gene', 'Pattern_Gene', 'Transcription_Factor', 'Pattern_TF', 'Reg'.")
    parser.add_argument("-gct", "--genes_per_gene_pattern_count_table", help = "Path to the file containing the count of genes per gene's pattern.")
    parser.add_argument("-or", "--output_relations_file", help = "Path to the output file (table containing transcription factor gathered by gene's pattern groups).")
    parser.add_argument("-oc", "--output_count_table", help = "Path to the new genes per genes' profiles count table, columns' labels must be : 'Pattern_Gene', 'Nb_genes_per_gene_pattern'.")
    parser.add_argument("-max", "--max_genes_per_pattern_number", help = "The maximum number of gene per gene's pattern authorizted to allow group with the opposed pattern.")
    parser.add_argument("--delete_small_gene_patterns", help = "Deletes instead of group gene's patterns with its opposed. Give the gene count threshold.")
    parser.add_argument("--gene_patterns_to_delete", help = "Uninteresting pgenes' atterns to delete (as constant patterns).")
    parser.add_argument("--TF_patterns_to_delete", help = "Uninteresting TF's patterns to delete (as constant patterns).")
    parser.add_argument("--follow_process_path", help = "Path to the file to save process data (markdown file, .md)..")
    parser.add_argument("--min_digit", help = "The minimum digit authorized in patterns (1 to 8).")
    parser.add_argument("--max_digit", help = "The maximum digit authorized in patterns (2 to 9).")
    parser.add_argument("-s", "--sep_table", help = "String that separates columns in tables, examples : '\t', ',', ....")

    args = parser.parse_args()

    group_pattern(args.max_digit, args.min_digit, args.follow_process_path, args.input_file, args.genes_per_gene_pattern_count_table, args.output_count_table, args.output_relations_file, args.max_genes_per_pattern_number, args.delete_small_gene_patterns, args.gene_patterns_to_delete, args.sep_table, args.TF_patterns_to_delete)