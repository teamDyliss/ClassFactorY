#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author : Ève Barré

--> General description : 

This script attributes a score to each TF from a list given from their annotations (on GO terms, Uniprot keywords, quotations number and percentages).
For each annotaiton type, three percision level defining the biological context (each having three points level). 
It has been applied a coefficient of 4 for points given with GO terms and Uniprot keywords because quotations percentage have 4 assess axes 
(in order to provides same weigth to each annotations categories in the score).
This score definition can be tougth again to be more flexible.

--> Arguments : 

TF_annotated_table : Path to the file containing TF' informations.
keywords_dic : Dictionnary of keywords's categories as keys and list of keywords of interest more (first) and less (last) precise in a list
GO_interest_list : list of GO terms (and their count of TF in GO) of interest list (first list = well known, second list : known, last list : described
keywords_operator : Operation to apply between keywords level scores, must be AND or OR. AND : if scores are added, OR if the best score (the first in the list) is keeped.
GO_operator : Operation to apply between GO terms level scores, must be AND or OR. AND : if scores are added, OR if the best score (the first in the list) is keeped.
strict_quot_thresholds : List of intervals and associated scores for quotations number in the strict condition, ex : [[thres_1, thresh_2], [score_1, score_2, score_3]]
strict_quot_perc_thresholds : List of intervals and associated scores for quotations number percentage in the strict condition, ex : [[thres_1, thresh_2], [score_1, score_2, score_3]]
large_quot_thresholds : List of intervals and associated scores for quotations number in the large condition, ex : [[thres_1, thresh_2], [score_1, score_2, score_3]]
large_quot_perc_thresholds : List of intervals and associated scores for quotations number percentage in the large condition, ex : [[thres_1, thresh_2], [score_1, score_2, score_3]]
follow_process_path : Path to the file to save process data.
sep_table : String that separates columns in tables.
TF_scored : Path to the TF scored table.

--> Proceeding : 
 
- defining the maximum possible score with calculate_max_score()
- calculate each TF score   
    - for each researched annotations : 
        - if operator is 'AND' : add each level score defined if the annotation is associated to the TF,
        - if operator is 'OR' : provide the maximum score of the most precise annotation associated to the TF (the first of the list)

Remarque : 
each annotation researched are given as a list of list of the current level keywords and the score to give,
thus, use can chose annotations to use, levels number and score associated to each case. 

--> In pipeline : 

This script must be lauched after having research each TF annotations and user must think about 
annotations precision level in input. 

--> Usage tips : 

See example with B cell differentiation. 

'''

import pandas as pd
import argparse 
import time 
import datetime 
import os
import matplotlib.pyplot as plt
from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage
import ast 
import pipeline 

# function needed 

def calculate_max_score(keywords_dic, keywords_operator, GO_interest_list, GO_operator, strict_quot_thresholds, strict_quot_perc_thresholds, large_quot_thresholds, large_quot_perc_thresholds):
    '''
    '''

    max_score = 0

    if keywords_dic : 
        if keywords_operator == "AND" : 
            max_score += sum([sum([keywords_and_score[1] for keywords_and_score in keywords_category_list]) for keywords_category_list in keywords_dic.values()])
        elif keywords_operator == "OR" :
            max_score += sum([keywords_category_list[0][1] for keywords_category_list in keywords_dic.values()])  # just best scores
        else : 
            raise ValueError(f"{keywords_operator} must be AND or OR.")
    
    if GO_interest_list : 
        if GO_operator == "AND" :
            max_score += sum([GO_list_and_score[1] for GO_list_and_score in GO_interest_list])
        elif GO_operator == "OR" :
            max_score += GO_interest_list[0][1]  # just the best possible score, the first written in the list 
        else : 
            raise ValueError(f"{GO_operator} must be AND or OR.")

    for quotations_arguments in [strict_quot_thresholds, strict_quot_perc_thresholds, large_quot_thresholds, large_quot_perc_thresholds] :
        max_score += quotations_arguments[1][-1]
    
    return max_score


def score_TF_annotations(row, keywords_dic, keywords_operator, GO_interest_list, GO_operator, strict_quot_thresholds, strict_quot_perc_thresholds, large_quot_thresholds, large_quot_perc_thresholds) :
    '''
    '''
    score = 0

    # multiply points by four for keywords and GO annotations score because for quotations, 4 ways to have points (large, strict for perc and number)

    # classifiying on keywords :
    if keywords_dic :
        for keyword_cat in keywords_dic.keys() :  # for each keyword category (biological process, disease...)

            if (str(row[keyword_cat]) == "nan") | (row[keyword_cat] == None):  # If no keyword 
                break 
            
            if keywords_operator == "AND" :  # add each points if the category is found 
                for keywords_list in keywords_dic[keyword_cat] :
                    if any([keyword in row[keyword_cat]] for keyword in keywords_list[0]) :
                        score += keywords_list[1]
            
            elif keywords_operator == "OR" :  # give just the best score if the more precised level is found 
                for keywords_list in keywords_dic[keyword_cat] :
                    if any([keyword in row[keyword_cat]] for keyword in keywords_list[0]) :
                        score += keywords_list[1]  
                        break 
            else : 
                raise ValueError(f"{keywords_operator} must be AND or OR.")
            

    # classifiying on GO_terms : 
    if GO_interest_list :  # [[[GO_1, GO_2, ...], score_1], [[GO_3, GO_4, ...], score_2], ...]
        
        if GO_operator == "AND" :
            for GO_interest in GO_interest_list :  
                if any([row[GO] == "X" for GO in GO_interest[0]]) :
                    score += GO_interest[1]  # == given score
                        
        elif GO_operator == "OR" :
            for GO_interest in GO_interest_list : 
                if any([row[GO] == "X" for GO in GO_interest[0]]) :
                    score += GO_interest[1]  # == given score
                    break
            
        else : 
            raise ValueError(f"{GO_operator} must be AND or OR.")


    # classifying on quotations numbers and percentages : 
    for threshold, name_threshold in zip([strict_quot_thresholds, strict_quot_perc_thresholds, large_quot_thresholds, large_quot_perc_thresholds], ["strict_quotations_nb", "strict_quote_per", "large_quotations_nb", "large_quote_per"]) :
        if threshold :
            # [[thres_1, thresh_2], [score_1, score_2, score_3]], level number is free
            #  example : [[5, 10], [1, 2, 3]]
            #  1 point for interval ]0; 5]
            #  2 point for interval ]5; 10]
            #  3 point for interval > 10 
            if row[name_threshold] == 0 :
                continue
            for thresholds_id in range(0, (len(threshold[1]) - 1)) :  # one less thresholds than intervals 
                score_old = score 
                if row[name_threshold] <= threshold[0][thresholds_id] :  # because argument are always str
                    score += threshold[1][0]
            if score_old == score : 
                score += threshold[1][-1]
            score_old = None           

    return {"TF_annotations_score" : score}


def TF_classification(sep_table, TF_scored, follow_process_path, TF_annotated_table, keywords_dic, keywords_operator, GO_interest_list, GO_operator, strict_quot_thresholds, strict_quot_perc_thresholds, large_quot_thresholds, large_quot_perc_thresholds) :
    '''
    '''

    t0 = time.time()

    # Checks if arguments are missing :
    for argument in [sep_table, follow_process_path, TF_scored, TF_annotated_table] :
        if not argument : 
            raise ValueError(f"{argument} is missing.")

    # Checks input files' existance :
    if not os.path.exists(TF_annotated_table):
        raise ValueError(f"{TF_annotated_table} does not exist. Change input.")
    
    if not os.path.exists("/".join(TF_scored.split("/")[:-1])):
        os.makedirs("/".join(TF_scored.split("/")[:-1]))

    # asserts is a .md file 
    if follow_process_path[-3:] != ".md" :
        raise ValueError(f"'follow_process_path' must be a markdown (.md) file not {follow_process_path.split('.')[-1]}.")

    follow_process_dir = "/".join(follow_process_path.split("/")[:-1])

    if not os.path.exists(follow_process_dir):
        os.makedirs(follow_process_dir)

    if not keywords_dic : 
        raise ValueError("keywords_dic file missing. (--keywords_dic)")
    
    strict_quot_thresholds = ast.literal_eval(str(strict_quot_thresholds))
    strict_quot_perc_thresholds = ast.literal_eval(str(strict_quot_perc_thresholds))
    large_quot_thresholds = ast.literal_eval(str(large_quot_thresholds))
    large_quot_perc_thresholds = ast.literal_eval(str(large_quot_perc_thresholds))
    keywords_dic = ast.literal_eval(str(keywords_dic))
    GO_interest_list = ast.literal_eval(str(GO_interest_list))

    # markdown file to follow process :
    text_info = (f"""

# <font color=orange>Module TF_classification.py :</font> 

lauched at {datetime.datetime.today()}

with arguments : 

| Arguments | |
| --- | --- |
| TF_annotated_table | {TF_annotated_table} |
| keywords_dic | {keywords_dic} |
| GO_interest_list | {GO_interest_list} |
| keywords_operator | {keywords_operator} |
| GO_operator | {GO_operator} |
| strict_quot_thresholds | {strict_quot_thresholds} |
| strict_quot_perc_thresholds | {strict_quot_perc_thresholds} |
| large_quot_thresholds | {large_quot_thresholds} |
| large_quot_perc_thresholds | {large_quot_perc_thresholds} |
| follow_process_path | {follow_process_path} |
| sep_table | {sep_table} |
| TF_scored | {TF_scored} | 

""")


    print(f"""
********************************************************************************
--> Module TF_classification.py :
lauched at {datetime.datetime.today()}
********************************************************************************
""")

    with open(follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text_info)

    input_file_size = os.path.getsize(TF_annotated_table)

    # TF table importation : 
    TF_table = pd.read_csv(TF_annotated_table, sep = sep_table, header = 0) #, engine = "python")

    # print(TF_table.head())
    
    # calculating score with the function score_TF_annotations :
    max_score =  calculate_max_score(keywords_dic, keywords_operator, GO_interest_list, GO_operator, strict_quot_thresholds, strict_quot_perc_thresholds, large_quot_thresholds, large_quot_perc_thresholds)
    TF_table[f"TF_annotations_score_on_{max_score}"] = TF_table.apply(lambda row : score_TF_annotations(row, keywords_dic, keywords_operator, GO_interest_list, GO_operator, strict_quot_thresholds, strict_quot_perc_thresholds, large_quot_thresholds, large_quot_perc_thresholds), axis = 1, result_type = "expand")

    # sort on bests scores : score at 0 are new TF
    TF_table = TF_table.sort_values(by = f"TF_annotations_score_on_{max_score}", ascending = False)

    # store TF_table : 
    TF_table.to_csv(TF_scored, sep = "\t", index = None)  # sep_table

    # scores ditributions 

    pipeline.stat(TF_table[[f"TF_annotations_score_on_{max_score}"]], f"TF_annotations_score_on_{max_score}", "hist, box", "\t", f"{follow_process_dir}/graphics")

    text_info = f"""


## Statistics on TF_annotations_score_on_{max_score} : 
***

{pipeline.display_table_stat(f"graphics/TF_annotations_score_on_{max_score}_hist.png", f"TF_annotations_score_on_{max_score}_hist", f"graphics/TF_annotations_score_on_{max_score}_box.png" , f"TF_annotations_score_on_{max_score}_box", TF_table[[f"TF_annotations_score_on_{max_score}"]].describe().to_markdown())}

***

{pipeline.makes_percentiles_intervals(TF_table[["Transcription_Factor", f"TF_annotations_score_on_{max_score}"]], f"TF_annotations_score_on_{max_score}").to_markdown()}


***
""" 

    # Execution time 
    print(f"Ends at {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")
    text_info += f"\nExecution time : {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes"

    with open(follow_process_path, "a") as info_file:
        info_file.write(text_info)
    


    return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(input_file_size / 1024)]

if __name__ == "__main__" : 

    parser = argparse.ArgumentParser()

    parser.add_argument("--TF_annotated_table", help = "Path to the file containing TF' informations." )
    parser.add_argument("--keywords_dic", help = "Dictionnary of keywords's categories as keys and list of keywords of interest more (first) and less (last) precise in a list")
    parser.add_argument("--GO_interest_list", help = "list of GO terms (and their count of TF in GO) of interest list (first list = well known, second list : known, last list : described")
    parser.add_argument("--keywords_operator", help = "Operation to apply between keywords level scores, must be AND or OR. AND : if scores are added, OR if the best score (the first in the list) is keeped.")
    parser.add_argument("--GO_operator", help = "Operation to apply between GO terms level scores, must be AND or OR. AND : if scores are added, OR if the best score (the first in the list) is keeped.")
    parser.add_argument("--strict_quot_thresholds", help = "List of intervals and associated scores for quotations number in the strict condition, ex : [[thres_1, thresh_2], [score_1, score_2, score_3]]")
    parser.add_argument("--strict_quot_perc_thresholds", help = "List of intervals and associated scores for quotations number percentage in the strict condition, ex : [[thres_1, thresh_2], [score_1, score_2, score_3]]")
    parser.add_argument("--large_quot_thresholds", help = "List of intervals and associated scores for quotations number in the large condition, ex : [[thres_1, thresh_2], [score_1, score_2, score_3]]")
    parser.add_argument("--large_quot_perc_thresholds", help = "List of intervals and associated scores for quotations number percentage in the large condition, ex : [[thres_1, thresh_2], [score_1, score_2, score_3]]")
    parser.add_argument("--follow_process_path", help = "Path to the file to save process data.")
    parser.add_argument("-s", "--sep_table", help = "String that separates columns in tables.")
    parser.add_argument("--TF_scored", help = "Path to the TF scored table.")

    args = parser.parse_args()

    TF_classification(args.sep_table, args.TF_scored, args.follow_process_path, args.TF_annotated_table, args.keywords_dic, args.keywords_operator, args.GO_interest_list, args.GO_operator, args.strict_quot_thresholds, args.strict_quot_perc_thresholds, args.large_quot_thresholds, args.large_quot_perc_thresholds)

    
    # keywords : {"Biological process" : [[["Adaptive immunity", "Differentiation", "Adaptive immunity", "Innate immunity", "Antiviral defense", "Inflammatory response", "Host-virus interaction", "Immunity", "unfolded protein response"], 8], [["Cell cycle", "Apoptosis", "Stress response"], 4]]}
    # GO : [[["3_B cell mediated immunity", "30_B cell activation", "8_B cell differentiation", "0_plasma cell differentiation"], 4], [["69_lymphocyte activation", "11_adaptive immune response"], 4], [["645_cellular developmental process", "161_immune system process"], 4]]
    # strict_quot_thresholds : [[3, 14], [1, 2, 3]]
    # strict_quot_perc_thresholds : [[3, 10], [1, 2, 3]]
    # large_quot_thresholds : [[16, 63], [1, 2, 3]]
    # large_quot_perc_thresholds : [[3, 10], [1, 2, 3]]