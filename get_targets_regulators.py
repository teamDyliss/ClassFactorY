#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author : Ève Barré

--> General description : 

Get_targets_regulators describes each gene interactions with other genes in the network. 
The output is a table of each gene regulators (transcription factors that target it positively or negatively) 
and targets (if the gene is a transcription factor). 
If you are particularly interested in a candidate gene, you can have a first glance of its relations here.


--> Arguments :

    "--relations_table" : Path to the file containing relations' data (must have as columns labels : 'Transcription_Factor', 'Pattern_Gene', 'Pattern_TF', 'Reg').
    "--Genes" : Genes symbole name of genes of interest.
    "--follow_process_path" : Path to the file to save process data.
    "--result_path" : Path to the file to save relations process selected.
    "-s", "--sep_table" : String that separates columns in transcription table of interest, example : '\t', ',', ....

--> Proceeding : 

- Imports relations_table (relations_table)
- Selects genes of interest relations (if Genes)
- Defines gene's targets and regulators for both regulatory direction
- Renames to merge with same column label 'Gene'
- Merges on 'Gene'
- Adds regulators and targets counts
- Selects relations involving genes of interest (because other of gene of interest can be still in the table
- Modifies columns order 
- Stores result's table
- writes in a file (follow_process_path) following informations  on query : 
    - Module name 
    - Date and time of launch
    - Arguments 
    - Statistics on nb regulators and targets per genes (positives and negativs)


--> In pipeline : 
get_targets_regulators helps to have a first or a last glance on genes (or TF) and on which they interact together.

--> Usage tips : 
It would be very fast to query for all Genes so, select Genes of interest is not the general use. 

--> Execution time and memory evolution : 
Execution time is very fast. 

'''


import pandas as pd
import argparse 
import os
import time
import datetime
import matplotlib.pyplot as plt
from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage
import pipeline

# funciton needed to use 'apply' : 

def add_counts(row:{}) :
    '''
    '''

    new_col_count = {}
    if str(row["pos_regulators"]) != "nan" : 
        new_col_count["pos_regulators_nb"] = len(row["pos_regulators"].split(", "))
    else : 
        new_col_count["pos_regulators_nb"] = 0

    if str(row["neg_regulators"]) != "nan" : 
        new_col_count["neg_regulators_nb"] = len(row["neg_regulators"].split(", "))
    else : 
        new_col_count["neg_regulators_nb"] = 0

    if str(row["pos_targets"]) != "nan" : 
        new_col_count["pos_targets_nb"] = len(row["pos_targets"].split(", "))
    else : 
        new_col_count["pos_targets_nb"] = 0

    if str(row["neg_targets"]) != "nan" : 
        new_col_count["neg_targets_nb"] = len(row["neg_targets"].split(", "))
    else : 
        new_col_count["neg_targets_nb"] = 0

    return new_col_count


def get_targets_regulators(follow_process_path, relations_table, Genes, result_path, sep_table:str) :
    '''
    '''

    t0 = time.time()

    # asserts is a .md file 
    if follow_process_path[-3:] != ".md" :
        raise ValueError(f"'follow_process_path' must be a markdown (.md) file not {follow_process_path.split('.')[-1]}.")

    # markdown file to follow process :
    text_info = (f"""

# <font color=orange>Module get_targets_regulators.py :</font> 

lauched at {datetime.datetime.today()}

with arguments : 

| Arguments | |
| --- | --- |
| relations_table | {relations_table} |
| follow_process_path | {follow_process_path} |
| Genes | {Genes} |
| result_path | {result_path} |
| sep_table | {sep_table} |

""")
    print(f"""

********************************************************************************
--> Module get_targets_regulators.py :
lauched at {datetime.datetime.today()}
********************************************************************************

""")

    # Checks existence of input files : 
    if not os.path.exists(relations_table):
        raise ValueError(f"{relations_table} does not exist. Change input.")

    if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
        os.makedirs("/".join(follow_process_path.split("/")[:-1]))
    
    follow_process_dir = "/".join(follow_process_path.split("/")[:-1])

    with open(follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text_info)

    input_file_size = os.path.getsize(relations_table)

    relations_table = pd.read_csv(relations_table, sep = sep_table, header = 0, usecols = ["Transcription_Factor", "Gene", "Pattern_TF", "Pattern_Gene", "Reg"]).drop_duplicates()

    # selects genes of interest relations : 
    if Genes : 
        selected_relations_table = pd.DataFrame({})
        for c_gene in Genes.split(", ") :
            selected_relations_table = selected_relations_table.append(relations_table[relations_table["Gene"] == c_gene])
            selected_relations_table = selected_relations_table.append(relations_table[relations_table["Transcription_Factor"] == c_gene])

        relations_table = selected_relations_table
        selected_relations_table = pd.DataFrame({})
    
    relations_table["Gene_Pattern_Gene"] = relations_table["Gene"] + "_" + relations_table["Pattern_Gene"].astype(str)
    relations_table["TF_Pattern_TF"] = relations_table["Transcription_Factor"] + "_" + relations_table["Pattern_TF"].astype(str)

    relations_table = relations_table[["Gene_Pattern_Gene", "TF_Pattern_TF", "Reg"]]

    # Defines gene's targets and regulators for both regulatory direction :
    pos_regulators = relations_table[relations_table["Reg"] == "+"][["Gene_Pattern_Gene", "TF_Pattern_TF"]].groupby(["Gene_Pattern_Gene"])["TF_Pattern_TF"].agg(", ".join).reset_index(name = "pos_regulators")
    neg_regulators = relations_table[relations_table["Reg"] == "-"][["Gene_Pattern_Gene", "TF_Pattern_TF"]].groupby(["Gene_Pattern_Gene"])["TF_Pattern_TF"].agg(", ".join).reset_index(name = "neg_regulators")

    pos_targets = relations_table[relations_table["Reg"] == "+"][["Gene_Pattern_Gene", "TF_Pattern_TF"]].groupby(["TF_Pattern_TF"])["Gene_Pattern_Gene"].agg(", ".join).reset_index(name = "pos_targets")
    neg_targets = relations_table[relations_table["Reg"] == "-"][["Gene_Pattern_Gene", "TF_Pattern_TF"]].groupby(["TF_Pattern_TF"])["Gene_Pattern_Gene"].agg(", ".join).reset_index(name = "neg_targets")

    # Renames to merge with same column label 'Gene' : 
    pos_regulators = pos_regulators.rename(columns = {"Gene_Pattern_Gene" : "Gene"})
    neg_regulators = neg_regulators.rename(columns = {"Gene_Pattern_Gene" : "Gene"})
    pos_targets = pos_targets.rename(columns = {"TF_Pattern_TF" : "Gene"})
    neg_targets = neg_targets.rename(columns = {"TF_Pattern_TF" : "Gene"})

    # Merges on 'Gene' :
    Global_table = pos_regulators.merge(neg_regulators, how = "outer")
    Global_table = Global_table.merge(pos_targets, how = "outer")
    Global_table = Global_table.merge(neg_targets, how = "outer")

    # adds regulators and targets counts : 
    add_table = Global_table.apply(lambda row : add_counts(row), axis = 1, result_type = "expand")
    Global_table = Global_table.merge(add_table, left_index = True, right_index = True)
    add_table = []

    # Selects relations involving genes of interest :
    if Genes :  
        for c_gene in Genes.split(", ") :
            for c_gene_pattern in [value for value in Global_table["Gene"] if ((value.split("_")[0]) == c_gene)] :
                selected_relations_table = selected_relations_table.append(Global_table[Global_table["Gene"] == c_gene_pattern])
    
        Global_table = selected_relations_table

    # get columns order : 
    Global_table = Global_table[["Gene", "pos_regulators", "pos_regulators_nb", "neg_regulators", "neg_regulators_nb", "pos_targets", "pos_targets_nb", "neg_targets", "neg_targets_nb"]]

    # stores result's table : 
    Global_table.to_csv(result_path, sep = sep_table, index = None)

    pipeline.stat(Global_table[["Gene", "pos_regulators_nb"]], "pos_regulators_nb", "hist, box", "\t", f"{follow_process_dir}/graphics")
    pipeline.stat(Global_table[["Gene", "neg_regulators_nb"]], "neg_regulators_nb", "hist, box", "\t", f"{follow_process_dir}/graphics")
    pipeline.stat(Global_table[(Global_table["neg_targets_nb"] != 0) | (Global_table["pos_targets_nb"] != 0)][["Gene", "pos_targets_nb"]], "pos_targets_nb", "hist, box", "\t", f"{follow_process_dir}/graphics")
    pipeline.stat(Global_table[(Global_table["neg_targets_nb"] != 0) | (Global_table["pos_targets_nb"] != 0)][["Gene", "neg_targets_nb"]], "neg_targets_nb", "hist, box", "\t", f"{follow_process_dir}/graphics")

    text_info = f"""



## pos_regulators_nb : 
***

{pipeline.display_table_stat("graphics/pos_regulators_nb_hist.png", "pos_regulators_nb_hist", "graphics/pos_regulators_nb_box.png" , "pos_regulators_nb_box", Global_table[["Gene", "pos_regulators_nb"]].describe().to_markdown())}

***

## neg_regulators_nb : 


{pipeline.display_table_stat("graphics/neg_regulators_nb_hist.png", "neg_regulators_nb_hist", "graphics/neg_regulators_nb_box.png", "neg_regulators_nb_box", Global_table[["Gene", "neg_regulators_nb"]].describe().to_markdown())}

***

## pos_targets_nb (for transcription factors only):    


{pipeline.display_table_stat("graphics/pos_targets_nb_hist.png", "pos_targets_nb_hist", "graphics/pos_targets_nb_box.png", "pos_targets_nb_box", Global_table[["Gene", "pos_targets_nb"]].describe().to_markdown())}

***

## neg_targets_nb (for transcription factors only) :    

{pipeline.display_table_stat("graphics/neg_targets_nb_hist.png", "neg_targets_nb_hist", "graphics/neg_targets_nb_box.png", "neg_targets_nb_box", Global_table[["Gene", "neg_targets_nb"]].describe().to_markdown())}

***

## targets_regulators head : 


{Global_table.head().to_markdown()}


"""




    # Execution time 
    print(f"Ends at {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")
    text_info += f"\nExecution time : {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes"

    with open(follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text_info)
    
    # execution_time (sec), execution_time (min), memory_usage (MiB), relations_table_size (MiB)
    return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(input_file_size / 1024)]

if __name__ == "__main__" : 

    parser = argparse.ArgumentParser()

    parser.add_argument("--relations_table", help = "Path to the file containing relations' data." )
    parser.add_argument("--Genes", help = "Gene symbole name of gene of interest." )
    parser.add_argument("--follow_process_path", help = "Path to the file to save process data.")
    parser.add_argument("--result_path", help = "Path to the file to save relations process selected.")
    parser.add_argument("--sep_table", help = "String that separates columns in transcription table of interest, example : '\t', ',', ....")

    args = parser.parse_args()

    get_targets_regulators(args.follow_process_path, args.relations_table, args.Genes, args.result_path, args.sep_table) 